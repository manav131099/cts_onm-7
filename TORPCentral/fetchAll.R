rm(list = ls())

errHandle = file('/home/admin/Logs/LogsTORPCentral.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/MasterMail/timestamp.R')
require('httr')
checkDir = function(x)
{
	if(!file.exists(x))
		dir.create(x)
}
createParent = function(day,path)
{
	DDMMYYYY = unlist(strsplit(as.character(day),"/"))
	pathYr = paste(path,DDMMYYYY[3],sep="/")
	checkDir(pathYr)
	pathMon = paste(pathYr,paste(DDMMYYYY[3],"-",DDMMYYYY[2],sep=""),sep="/")
	checkDir(pathMon)
	pathFinal = paste(pathMon,paste(paste(DDMMYYYY[3],DDMMYYYY[2],DDMMYYYY[1],sep="-"),".txt",sep=""),sep="/")
	return(pathFinal)
}

timetomins = function(x)
{
	splitup = unlist(strsplit(x,":"))
	hr = as.numeric(splitup[1])
	min = as.numeric(splitup[2])
	return(((hr * 60)+min+1))
}

fetchrawdata = function(day1,day2)
{
  	#fetch data from SERVER USING A POST-request
		fileWritePath = createParent(day1,pathCentral)
		while(1)
		{
		fetchdata = try(httr::POST(
  	paste('http://35.165.7.5/torp/ServiceRouter/getpwrplantdata?starttime=',day1,'&endtime=',day2,'&showinvdata=1',sep="")),silent=T)
		if(class(fetchdata)!='try-error')
			break
		print('Error in server, will try again in 5 mins')
		Sys.sleep(300)
		}
		print('Data fetched')
  	#get the data of the fetched data using the content function
  	recordTimeMaster("TORPCentral","FTPProbe")
		vals = try(content(fetchdata),silent=T)

		############################################################################
		#the data can be fetched only when logged into the server, the login is 
		#handled in Live.R, however should the login, fail we have another fail-safe
		# mechanism, which is handled below
		############################################################################
		if(class(vals) == 'try-error')
		{
			print('Forcing login again')
			req = httr::POST("http://35.165.7.5/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")
  		fetchdata = httr::POST(
  		paste('http://35.165.7.5/torp/ServiceRouter/getpwrplantdata?starttime=',day1,'&endtime=',day2,'&showinvdata=1',sep=""))
			print('Data fetched again')
  		vals = content(fetchdata)
		}

		#All the data is stored within the powerplant sub-vector.
		vals2 = vals$pwrplantdata
		
		############################################################################
		#extract the column-names of raw data, for this purpose we save the first
		#row of vals2 sub-vector as a data-frame. colnms stores the column names of
		#the raw data
		############################################################################

		colnms = data.frame(vals2[1])
		colnms = colnames(colnms)
		
		############################################################################
		#Since vals2 is a array of vectors and not a data-frame, we can't apply 
		#ordinary data-frame operations. The 13th column of the sub-vector has the 
		#station-names. Use the sapply call to fetch the 13th column across all rows
		#Data fetched using the post-call has data for all stations
		#(in-fact there are close to 23 stations data that's pumped live). To get
		#relevant data we need to find match for 'MJ-Logistics', which is the
		#identifier for the site name. There are 21 unique columns in the sub-vector
		#
		#So the data returned from the POST call is something as below:
		#
		#Col1		Col2		Col3......	Col13.......		Col21
		#DATA		DATA		DATA				MJ Logistics		DATA
		#DATA		DATA		DATA				Apolo-Tyres			DATA
		#DATA		DATA		DATA				Power-Brakes		DATA
		#DATA		DATA		DATA				MJ Logistics		DATA
		#
		#Now we need to extract only those rows where column 13 is MJ-Logistics
		#This is achieved below
		###########################################################################
		stnnames = sapply(vals2,`[`,13)
		stnnames =  unlist(stnnames)
		stnnames = as.character(stnnames)
  	uniq = 21 # No of columns present in the sub-vector

		#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		#IMPORTANT: IF YOU NEED TO ADD A NEW STATION, CHANGE THE STRING AFTER THE 
		# '%' SIGN TO REFLECT THE CORRESPONDING STATION NAME
		#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	#	subst = which(stnnames %in% "MJ Logistics")
	#	vals2 = vals2[subst] # extract only rows with MJ Logistics tag in column 13
		
		#Now once we have the rows construct a data-frame to store raw-data values
		datalist = vector('list',uniq)
		for(outer in 1 : uniq)
		{
			temp = sapply(vals2,`[`,outer)
			temp =  unlist(temp)
			temp = as.character(temp)
			datalist[[outer]] = temp
		}
  	#return the data frame
		df = data.frame(datalist)
  	colnames(df)=colnms
		write.table(df,file=fileWritePath,sep="\t",row.names = F,col.names = T,append =F)
		#return(df)
		print('Raw data fetched')
}

updateLastDate= function(day1,path)
{
	day1 = as.character(day1)
	day1 = unlist(strsplit(day1,"/"))
	day1 = paste(day1[3],day1[2],day1[1],sep="-")
	write(day1,file = path,append=F)
}

managebacklog = function(pathlastday)
{
	today = as.character(Sys.Date())
	nowdate = unlist(strsplit(as.character(today),"-"))

	#Get year month and date for current day
	yrnow = as.numeric(nowdate[1])
	mnnow = as.numeric(nowdate[2])
	dynow = as.numeric(nowdate[3])


	print(paste('Now',yrnow,mnnow,dynow))

	#Read content of the pathlastday file to get last recorded date
	lastdate = readLines(pathlastday)


	newdate = unlist(strsplit(as.character(lastdate),"-"))
	
	#Get year month and date for last-recorded date
	yrlast = as.numeric(newdate[1])
	mnlast = as.numeric(newdate[2])
	dylast = as.numeric(newdate[3])


	print(paste('Last',yrlast,mnlast,dylast))
	
	#Conditions for no backlog
	{
		if((yrnow==yrlast) && (mnnow==mnlast) && ((dynow -dylast) <= 1))
		{
			print('No Backlogs')
			return()
		}
		else if((yrnow==yrlast) && ((mnnow-1)==mnlast) && (dynow==1) && (dylast == daysmonthsno[mnlast]))
		{
				print('No Backlogs')
				return()
		}
	}

	print('backlog exist')
	print(paste('last day:',lastdate,'today:',today))
	
	#Fetch backlog data by traversing through all the days in-between last recorded
	#date and current date

	for(x in yrlast : yrnow)
	{
		for(y in mnlast : mnnow)
		{
			start = end = 1
			if(y == mnlast)
			{
				start = dylast + 1
			}
			{
				if(y == mnnow)
				{
					end = dynow-1
				}
				else
				{
					end = daysmonthsno[y]
				}
			}
			for(z in start : end)
			{
				day = z
				mon = y
				yr = x
				if(day < 10)
				{
					day = paste("0",day,sep="")
				}
				if(mon < 10)
				{
					mon = paste("0",mon,sep="")
				}

				#parameter passed to fetchraw data must be in DD/MM/YYYY format
				day1 = paste(day,mon,yr,sep="/")

				#while naming files the format is YYYY-MM-DD 
				
				#pathdayraw stores the path to store the raw data from the TORP system
				#this content is fetched from the fetchrawdata function
				
			 fetchrawdata(day1,day1)

				updateLastDate(day1,pathlastday)
			}
		}
	}
	print('Backlogs done')
}

pathCentral = '/home/admin/Data/TORP Data/AllStations'
pathLastDate = '/home/admin/Start/TORPCentral.txt'
monthsno = c("01","02","03","04","05","06","07","08","09","10","11","12")
daysmonthsno = c(31,28,31,30,31,30,31,31,30,31,30,31)

checkDir(pathCentral)
ctrbackoff = 0

if(file.exists(pathLastDate))
	managebacklog(pathLastDate)

while(1)
{
#Get system time and convert it to India time (System is based in the US, 
#hence convert it to IST). If the time is before 02:00 hrs enter doze mode and 
#sleep for an hour.
	recordTimeMaster("TORPCentral","Bot")
	time = Sys.time()
	time = format(time, tz="Asia/Calcutta",usetz=TRUE)
	time = as.character(time)
	time = unlist(strsplit(time,"\\ "))
	date = time[1]
	time = time[2]
	timeac = timetomins(time)
	if(timeac < 45)
	{
		print('In doze mode')
		ctrbackoff = 12
		Sys.sleep(1800)
#log-in after an hours of sleep
		req = httr::POST("http://35.165.7.5/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")
#go back to the beginning of the loop
		next
	}

	#ctrbackoff reached 12, hence relogin.
	if(ctrbackoff > 11)
	{
		print('Hit backoff making connection request...')
		req = httr::POST("http://35.165.7.5/torp/ServiceRouter/login?loginid=operations@cleantechsolar.com&pwd=torp1227")
		ctrbackoff = 0
		print('request done')
	}

	rfmt = unlist(strsplit(date,"-"))
	yr = rfmt[1]
	mnth = rfmt[2]
	day = rfmt[3]
	#reformating the date to match previously defined convention in managebacklog
	day1 = paste(rfmt[3],rfmt[2],rfmt[1],sep="/")
	fetchrawdata(day1,day1)
	updateLastDate(day1,pathLastDate)
	print('Sleeping for 5mins')
	Sys.sleep(300)
	
}
sink()

