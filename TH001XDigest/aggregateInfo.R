source('/home/admin/CODE/common/aggregate.R')

registerMeterList("TH-001X",c("SCSC-Load","SCSC-S1","SCSC-S2"))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = NA #column for LastRead
aggColTemplate[4] = NA #column for LastTime
aggColTemplate[5] = 4 #column for Eac-1
aggColTemplate[6] = NA #column for Eac-2
aggColTemplate[7] = NA #column for Yld-1
aggColTemplate[8] = NA #column for Yld-2
aggColTemplate[9] = NA #column for PR-1
aggColTemplate[10] = NA #column for PR-2
aggColTemplate[11] = NA #column for Irr
aggColTemplate[12] = NA # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("TH-001X","SCSC-Load",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 10 #column for LastRead
aggColTemplate[4] = 11 #column for LastTime
aggColTemplate[5] = 4 #column for Eac-1
aggColTemplate[6] = 5 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 7 #column for Yld-2
aggColTemplate[9] = 8 #column for PR-1
aggColTemplate[10] = 9 #column for PR-2
aggColTemplate[11] = 12 #column for Irr
aggColTemplate[12] = "TH-001C" # IrrSrc Value
aggColTemplate[13] = 13 #column for Tamb
aggColTemplate[14] = 14 #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("TH-001X","SCSC-S1",aggNameTemplate,aggColTemplate)

aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 10 #column for LastRead
aggColTemplate[4] = 11 #column for LastTime
aggColTemplate[5] = 4 #column for Eac-1
aggColTemplate[6] = 5 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 7 #column for Yld-2
aggColTemplate[9] = 8 #column for PR-1
aggColTemplate[10] = 9 #column for PR-2
aggColTemplate[11] = 12 #column for Irr
aggColTemplate[12] = "TH-001C" # IrrSrc Value
aggColTemplate[13] = 13 #column for Tamb
aggColTemplate[14] = 14 #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("TH-001X","SCSC-S2",aggNameTemplate,aggColTemplate)

