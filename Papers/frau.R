
rm(list = ls())
require("ggplot2")
#data = read.csv("/home/admin/Wan Pairs - V2.csv",header=T,stringsAsFactors=F)
data = read.csv("/home/admin/Shravan - Paper Final.csv",header=T,stringsAsFactors=F)
facs = unique(data$Region)
data$Region = factor(data$Region, levels = facs, labels = facs )       #to fix legend arrangement
data$Irradiation = as.numeric(gsub(",","",data$Irradiation))
data$Yield = as.numeric(gsub(",","",data$Yield))
unval = c("IN-N",
"IN-C",
"IN-S",
"KH",     
"MY",
"PH",
"SG",
"TH",     
"VN")
titlesettings <- theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
                  plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5))
p1 <- ggplot() + theme_bw()
#rightIdx = nrow(dff) * 0.6
p1 <- p1 + geom_point(data=data, aes(x=Irradiation, y =Yield, shape = Region, colour = Region), size = 4.5)
p1 <- p1 +  ylab("Annual Specific Yield [kWh/kWp]") + xlab("Annual Global Horizontal or Tilted Irradiation (satellite) [kWh/m2]") + coord_cartesian(ylim = c(1000,2000),xlim=c(1500,2200))
p1 <- p1 + scale_x_continuous(breaks=seq(from=1500,to=2200,by=100)) + scale_y_continuous(breaks=seq(from=1000, to=2000, by=100))
p1 <- p1 + theme(axis.title.x = element_text(size=17,face = "bold"), axis.title.y = element_text(size=17,face = "bold"),
                 axis.text = element_text(size=17), panel.grid.minor = element_blank(), panel.border = element_blank(),axis.line = element_line(colour = "black"),panel.grid.major.x=element_blank(),
                 legend.justification = c(0, 0.97), legend.position = c(0.03, 0.97),legend.background = element_rect(fill=alpha('blue', 0)))                     #LEGNED AT RHS
p1 <- p1 + titlesettings 
#p1 <- p1 + ggtitle('PR vs Yield', subtitle = paste0('For all'))
p1 <- p1 + theme(legend.text=element_text(size=15), legend.title=element_text(size=15))
p1 <- p1 + scale_colour_manual('', 
                               values = c("red", "red", "red","blue","green","orange","hotpink","sandybrown","violetred3"), 
                               labels = unval, guide = guide_legend(title.position = "left",nrow=9)) 
p1 <- p1 + scale_shape_manual('',
                              labels = unval,
                              values = c(0,7,15,1,2,3,4,5,11), guide = guide_legend(title.position = "left",nrow=9))
p1 <- p1 + theme(legend.box = "horizontal",legend.direction="horizontal") #legend.position = "bottom")

p1 <- p1 + geom_abline(intercept=0,slope=1,color="black",size=2)
p1 <- p1 + geom_abline(intercept=0,slope=0.9,color="black",linetype = "dashed")
p1 <- p1 + geom_abline(intercept=0,slope=0.8,color="black",linetype = "dashed")
p1 <- p1 + geom_abline(intercept=0,slope=0.7,color="black",linetype = "dashed")
p1 <- p1 + geom_abline(intercept=0,slope=0.6,color="black",linetype = "dashed")
p1 <- p1 + geom_abline(intercept=0,slope=0.5,color="black",linetype = "dashed")
#p1 <- p1 + geom_abline(intercept=0,slope=1,color="red")

#p1 <- p1 + geom_hline(yintercept = 72,colour="darkgreen",size=1) 
p1 <- p1 + annotate('text', label = "PR = 100%", fontface =2,y = 1995 , x = 1930, colour = "black",size=6) 

#"PR= 100%", y = 1900 , x = 1850, colour = "black")
p1 <- p1 + annotate('text', label = "90%", y = 1820 , x = 1980, colour = "black",fontface=2,size=6)
p1 <- p1 + annotate('text', label = "80%", y = 1660 , x = 2030, colour = "black",fontface=2,size=6)
p1 <- p1 + annotate('text', label = "70%", y = 1485 , x = 2080, colour = "black",fontface=2,size=6)
p1 <- p1 + annotate('text', label = "60%", y = 1310 , x = 2130, colour = "black",fontface=2,size=6)
p1 <- p1 + annotate('text', label = "50%", y = 1120 , x = 2180, colour = "black",fontface=2,size=6)
#p1 <- p1 + guides(colour = guide_legend(override.aes = list(shape = 18)))
p1 <- p1 + theme(legend.key.height=unit(1.75,"line"))
#p1 <- p1 + annotate("text",label="Heavy soiling",x=1580,y=1040,hjust=0,fontface=1,size=6)
#p1 <- p1 + annotate("text",label="Curtailment",x=1900,y=1050,hjust=0,fontface=1,size=6)
#p1 <- p1 + annotate("text",label="Trackers",x=1830,y=1780,hjust=0,fontface=1,size=6)
p1 <- p1 + theme(legend.key.height=unit(1.75,"line"))
p1 <- p1 + theme(legend.key.height=unit(1.75,"line"))
ggsave("/home/admin/Graphs/frau.pdf", p1, width = 11, height=8)


