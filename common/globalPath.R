#Don't un-comment the below line, unless you're running a standalone test
#rm(list = ls())

# Main Home
GLOB_HOME_PATH="/home/admin"

#Home dependent
GLOB_RAW_PATH=paste(GLOB_HOME_PATH,"Data",sep="/")
GLOB_DROPBOX_PATH=paste(GLOB_HOME_PATH,"Dropbox",sep="/")
GLOB_START_PATH=paste(GLOB_HOME_PATH,"Start",sep="/")
GLOB_LOGS_PATH=paste(GLOB_HOME_PATH,"Logs",sep="/")
GLOB_CODE_PATH=paste(GLOB_HOME_PATH,"CODE",sep="/")

#Raw dependent
GLOB_EBX_RAW_PATH=paste(GLOB_RAW_PATH,"Episcript-CEC",sep="/")

#Dropbox dependent
GLOB_EBX_GEN_1_PATH=paste(GLOB_DROPBOX_PATH,"Gen 1 Data",sep="/")
GLOB_SECOND_GEN_PATH=paste(GLOB_DROPBOX_PATH,"Second Gen",sep="/")
GLOB_THIRD_GEN_PATH=paste(GLOB_DROPBOX_PATH,"Third Gen",sep="/")
GLOB_FOURTH_GEN_PATH=paste(GLOB_DROPBOX_PATH,"Fourth_Gen",sep="/")
GLOB_FLEXI_PATH=paste(GLOB_DROPBOX_PATH,"FlexiMC_Data",sep="/")
GLOB_SERIS_GEN_1_PATH=paste(GLOB_DROPBOX_PATH,"Cleantechsolar","1min",sep="/")

#Start Dependent
GLOB_MASTER_MAIL_PATH=paste(GLOB_START_PATH,"MasterMail",sep="/")
GLOB_AGGREGATE_PATH=paste(GLOB_START_PATH,"Aggregate",sep="/")

#Flexi Dependent
GLOB_FLEXI_GEN_1_PATH=paste(GLOB_FLEXI_PATH,"Gen1_Data",sep="/")
GLOB_FLEXI_SECOND_GEN_PATH=paste(GLOB_FLEXI_PATH,"Second_Gen",sep="/")
GLOB_FLEXI_THIRD_GEN_PATH=paste(GLOB_FLEXI_PATH,"Third_Gen",sep="/")
GLOB_FLEXI_FOURTH_GEN_PATH=paste(GLOB_FLEXI_PATH,"Fourth_Gen",sep="/")

#Email Credentials
GLOB_SENDER = "operations@cleantechsolar.com" 
GLOB_USER_NAME = "shravan.karthik@cleantechsolar.com"
GLOB_PASSWORD = "CTS&*(789"

if(0)
{
checkdir = function(path)
{
	return(file.exists(path))
}
vars = ls()
for(x in 2 : length(vars))
{
	acVal = eval(parse(text = as.character(vars[x])))
	print(paste(vars[x],"---",acVal,"---",checkdir(acVal)))
}
}
