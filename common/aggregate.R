registerMeterList = function(stnId,meters)
{
basePath = "/home/admin/Start/Aggregate/"
writePath = paste(basePath,stnId,"_Meters.txt",sep="")
write(meters,writePath)
}

getNameTemplate = function()
{
	return(c("Date","DA","LastRead","LastTime","Eac-1","Eac-2","Yield-1","Yield-2","PR-1","PR-2","Irradiation","IrradiationSource","Tamb","Tmod","Hamb","IA","GA","PA"))
}

getColumnTemplate = function()
{
	val = getNameTemplate()
	val = unlist(rep(NA,length(val)))
	return(val)
}

registerColumnList = function(stnId,meter,name,colNo)
{
	basePath = "/home/admin/Start/Aggregate/"
	writePath = paste(basePath,stnId,"-",meter,"_Cols.txt",sep="")
	if(meter=="")
		writePath = paste(basePath,stnId,"_Cols.txt",sep="")
	df = data.frame(Name=name,Col=colNo,stringsAsFactors=F)
	write.table(df,file=writePath,row.names=F,col.names=T,sep="\t",append=F)
}

fetchNumericalExceptions = function()
{
	return(12)
}
