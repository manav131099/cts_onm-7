from flask import Flask, render_template, request, Response, jsonify
import requests
import pandas as pd
import itertools
from numpy.random import randint
import datetime
import re
import ast
import pandas as pd
import logging
import pandas as pd
import numpy as np
import json
import werkzeug._internal
import os

def demi_logger(type, message,*args,**kwargs):
    pass

path='/home/admin/Dropbox/Lifetime/Gen-1/'

# df_tags
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('Web_Interface_V2.html')


@app.route('/get_data',methods=['POST'])
def get_data():
    info = request.get_json(force=True)
    key=info['Key']
    print(key)
    if(key=='Cleantech@1'):
        stn=info['Id']
        date=info['Date']
        print(info)
        if(os.path.exists(path+stn+'-LT.txt')):
            df=pd.read_csv(path+stn+'-LT.txt',sep='\t')
            columns=df.columns.tolist()[1:-3]+[df.columns.tolist()[-1]]#No date,LastT,DA
            df_temp=df.loc[df['Date']==date,columns]
            if df_temp.empty:
                df_temp.loc[len(df_temp)] = 0
            df_temp=df_temp.fillna(0)
            print(df_temp.to_json(orient="records"))
            return Response(df_temp.to_json(orient="records"),status=200)
        else:
            return jsonify(['Site not automated']) 
    else:
        return jsonify(['Incorrect Secret Key'])


@app.route('/update_data',methods=['POST'])
def update_data():
    data = request.form.to_dict()
    if data['Date']=='' or data['Code']=='':
        return jsonify(['Failed'])
    df_temp=pd.DataFrame(data,index=[0])
    df_temp=df_temp.iloc[:,1:]
    df=pd.read_csv(path+data['Code']+'-LT.txt',sep='\t')
    columns=df.columns.tolist()[0:-3]+[df.columns.tolist()[-1]] #Add master flag
    df_temp=df_temp[columns] #reorder
    print(df_temp)
    df_check=df.loc[df['Date']==data['Date'],columns]
    print(df_temp.values)
    if df_check.empty:
        print('empty')
        df_temp.insert(len(columns)-1,'DA',0)
        df_temp.insert(len(columns)-1,'LastT',np.nan)
        print(df_temp)
        print(df_temp.columns)
        df_temp.to_csv(path+data['Code']+'-LT.txt',sep='\t',index=False,mode='a',header=False)
        return jsonify(['Success'])
    else:
        df.loc[df['Date']==data['Date'],columns]=df_temp.values.tolist()[0]
        df.to_csv(path+data['Code']+'-LT.txt',sep='\t',index=False)
        return jsonify(['Success'])

if __name__ == '__main__':
    werkzeug._internal._log = demi_logger
    app.run(host = '0.0.0.0',port=5000,debug=True)