import urllib.request, json
import csv,time
import datetime
import collections
import os
import pytz
import smtplib
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

#global declarations
ihead = ['i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20',
'i21','i22','i23','i24','i25','i26','i27','i28','i29','i30','i31','i32','i33','i34','i35','i36','i37','i38','i39','i40',
'i41','i42','i43','i44','i45','i46','i47','i48','i49','i50','i51','i52','i53','i54','i55','i56','i57','i58','i59','i60',
'i61','i62','i63','i64','i65','i66','i67','i68','i69','i70','i71','i72','i73','i74','i75','i76','i77','i78','i79','i80',
'i81','i82','i83','i84','i85','i86','i87']

mhead = ['m1','m2','m3','m4','m5','m6','m7','m8','m9','m10','m11','m12','m13','m14','m15','m16','m17','m18','m19','m20',
'm21','m22','m23','m24','m25','m26','m27','m28','m29','m30','m31','m32','m33','m34','m35','m36','m37','m38','m39','m40',
'm41','m42','m43','m44','m45','m46','m47','m48','m49','m50','m51','m52','m53','m54','m55','m56','m57','m58','m59','m60',
'm61','m62','m63','m64','m65','m66','m67']

whead = ['w1','w2','w3','w4','w5','w6','w7','w8','w9','w10','w11','w12','w13','w14','w15','w16','w17','w18','w19','w20','w21','w22','w23',
'w24','w25','w26','w27','w28','w29','w30','w31','w32','w33','w34','w35']



#Function to read start date
def read_date():
##    file = open('F:/Flexi_final/[IN-036C]/Raw/IN036C.txt','r') ###local
    file = open('/home/admin/Start/IN036C.txt','r') ###server
    date_read = file.read(10)
    return date_read

#Function to set start date
def set_date():
    date_read = read_date()
    global date
    date = datetime.datetime.strptime(date_read, '%Y-%m-%d').date()
    date += datetime.timedelta(days=-1) #To reduce date by a day as it gets added in current_date()

#Function to determine the next day
def determine_date():
    global date
    date += datetime.timedelta(days=1)
    return date


#Function to determine URL for the next day
def determine_url(date,url_head):
    replace_date = date.strftime("%Y%m%d")
    url = 'http://13.127.216.107/API/JSON/swl/Bescom/Data/'+replace_date+'/'+url_head+replace_date+'.json'
    return url

#Function to send mail
def sendmail():
    fromaddr = 'operations@cleantechsolar.com'
    toaddr = 'kn.rohan@gmail.com','andre.nobre@cleantechsolar.com','shravan1994@gmail.com','rupesh.baker@cleantechsolar.com'
    uname = 'shravan.karthik@cleantechsolar.com'

    subject = 'Bot IN-036C Raw Down - Portal Error'
    text = ' '
    message = 'Subject: {}\n\n{}'.format(subject,text)
    password = 'CTS&*(789'
    try:
        print('send try')
        server = smtplib.SMTP('smtp-mail.outlook.com',587)
        server.ehlo()
        server.starttls()
        server.login(uname,password)
        server.sendmail(fromaddr,toaddr,message)
        print('sending')
        server.quit()
    except Exception as e:
        print('mail send exception - ',str(e))



#Function to get header names
def get_header(json_head):
    if json_head == 'inverter':
        return ihead
    elif json_head == 'MFM':
        return mhead
    else:
        return whead


#Function to re-try connection
def connection_try(url):
    counter = 0
    while True:
        counter += 1
        try:
            print('second attempt try - ',counter)
            with urllib.request.urlopen(url) as url:
                try_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
                return
        except Exception as e:
            if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                if counter == 4:
                    sendmail()
                    exit('Exiting due to persistent connection error')
                    
                print('sleeping for 15')
                time.sleep(900)
            else:
                return


#Function to determine path and create directory 
def determine_path(curr_date,device,fd):
    year = curr_date.strftime("%Y")
    month = curr_date.strftime("%Y-%m")
    day = curr_date.strftime("%Y-%m-%d")
##    master_path = 'F:/del/FlexiMC_Data/Raw_Data/[IN-036C]/' ###local
    master_path = '/home/admin/Data/Flexi_Raw_Data/[IN-036C]/' ###server
    year_path = master_path+year+'/'
    month_path = year_path+month+'/'
    device_path = month_path+device+'/'
    final_path = device_path+'[IN-036C]-'+fd+'-'+day+'.txt'
    
    if not os.path.exists(year_path):
        os.makedirs(year_path)
    if not os.path.exists(month_path):
        os.makedirs(month_path)
    if not os.path.exists(device_path):
        os.makedirs(device_path)
    if os.path.exists(final_path):
        os.remove(final_path)
        print('[IN-036C]-'+fd+'-'+day+'.txt'+' removed')
    
    return final_path


#Function to determine path and create directory 
def meter(url_head,json_head,folder_name,file_field):
    tz = pytz.timezone('Asia/Kolkata')
    while True:
        today_date = datetime.datetime.now(tz).date()
        curr_date = determine_date()
        url = determine_url(curr_date,url_head)
        path = determine_path(curr_date,folder_name,file_field)
        if curr_date > today_date:
            print('Last file - Exiting while loop',str(curr_date))
            break
        try:
            with urllib.request.urlopen(url) as url:
                load_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
        except Exception as e:
            if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                print('Incomplete read/connection refuse error - 1  Slepping for 15 min - ',str(e))
                time.sleep(900)
                connection_try(url)
                try:
                    print('Last attempt try')
                    with urllib.request.urlopen(url) as url:
                        load_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
                except Exception as e:
                    if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                        print('Incomplete read/connection refuse error - 2  Killing bot and sending mail - ',str(e))
                        sendmail()
                        exit('Exiting due to persistent connection error')
                        
                    else:
                        if curr_date > today_date:
                            print('Last file -2 - Exiting while loop',str(curr_date), str(e))
                            break
                        else:
                            print('No data found -2 - ',str(curr_date), str(e))
                            continue                 
                    


            else:
                if curr_date > today_date:
                    print('Last file - Exiting while loop',str(curr_date), str(e))
                    break
                else:
                    print('No data found - ',str(curr_date), str(e))
                    continue
            
        new_file = 1
        i=0
        if load_data[json_head] == None:
            print('NULL Returned by Flexi')
            continue
        length = len(load_data[json_head])
        while i < length:
            data = load_data[json_head][i]
            with open(path, 'a',newline='') as csv_file:
                csvwriter = csv.writer(csv_file, delimiter='\t')
                line = []
                header = []
                
                header = get_header(json_head)
                line = [data.get(k,'NA') for k in header]
                line = ['NULL' if w is None else w for w in line]

                if new_file == 1:
                    csvwriter.writerow(header)
                    new_file = 0
                csvwriter.writerow(line)
                csv_file.close()
            i = i+1
        print(str(length)+' rows have been written to - '+path)



set_date()
meter('22_Inverter_1_','inverter','C_Inverter_1','CI1')
set_date()
meter('22_Inverter_2_','inverter','C_Inverter_2','CI2')

set_date()
meter('22_Inverter_3_','inverter','S_Inverter_1','SI1')
set_date()
meter('22_Inverter_4_','inverter','S_Inverter_2','SI2')
set_date()
meter('22_Inverter_5_','inverter','S_Inverter_3','SI3')
set_date()
meter('22_Inverter_6_','inverter','S_Inverter_4','SI4')
set_date()
meter('22_Inverter_7_','inverter','S_Inverter_5','SI5')
set_date() 
meter('22_Inverter_8_','inverter','S_Inverter_6','SI6')
set_date()
meter('22_Inverter_9_','inverter','S_Inverter_7','SI7')
set_date()
meter('22_Inverter_10_','inverter','S_Inverter_8','SI8')
set_date()
meter('22_Inverter_11_','inverter','S_Inverter_9','SI9') 
set_date()
meter('22_Inverter_12_','inverter','S_Inverter_10','SI10')
set_date()
meter('22_Inverter_13_','inverter','S_Inverter_11','SI11')
set_date()
meter('22_Inverter_14_','inverter','S_Inverter_12','SI12')
set_date()
meter('22_Inverter_15_','inverter','S_Inverter_13','SI13')
set_date()
meter('22_Inverter_16_','inverter','S_Inverter_14','SI14')
set_date()
meter('22_Inverter_17_','inverter','S_Inverter_15','SI15')
set_date()
meter('22_Inverter_18_','inverter','S_Inverter_16','SI16')
set_date()
meter('22_Inverter_19_','inverter','S_Inverter_17','SI17')
set_date()
meter('22_Inverter_20_','inverter','S_Inverter_18','SI18')
set_date()
meter('22_Inverter_21_','inverter','S_Inverter_19','SI19')
set_date()
meter('22_Inverter_22_','inverter','S_Inverter_20','SI20')
set_date()
meter('22_Inverter_23_','inverter','S_Inverter_21','SI21')
set_date()
meter('22_Inverter_24_','inverter','S_Inverter_22','SI22')
set_date()
meter('22_Inverter_25_','inverter','S_Inverter_23','SI23')
set_date()
meter('22_Inverter_26_','inverter','S_Inverter_24','SI24')
set_date()
meter('22_Inverter_27_','inverter','S_Inverter_25','SI25')
set_date()
meter('22_Inverter_28_','inverter','S_Inverter_26','SI26')
set_date()
meter('22_Inverter_29_','inverter','S_Inverter_27','SI27')
set_date()
meter('22_Inverter_30_','inverter','S_Inverter_28','SI28')
set_date()
meter('22_Inverter_31_','inverter','S_Inverter_29','SI29')
set_date()
meter('22_Inverter_32_','inverter','S_Inverter_30','SI30')
set_date()
meter('22_Inverter_33_','inverter','S_Inverter_31','SI31')
set_date()
meter('22_Inverter_34_','inverter','S_Inverter_32','SI32')
set_date()
meter('22_Inverter_35_','inverter','S_Inverter_33','SI33')
set_date()
meter('22_Inverter_36_','inverter','S_Inverter_34','SI34')
set_date()
meter('22_Inverter_37_','inverter','S_Inverter_35','SI35')
set_date()
meter('22_Inverter_38_','inverter','S_Inverter_36','SI36')
set_date()
meter('22_Inverter_39_','inverter','S_Inverter_37','SI37')
set_date()
meter('22_Inverter_40_','inverter','S_Inverter_38','SI38')
set_date()
meter('22_Inverter_41_','inverter','S_Inverter_39','SI39')
set_date()
meter('22_Inverter_42_','inverter','S_Inverter_40','SI40')
set_date()
meter('22_Inverter_43_','inverter','S_Inverter_41','SI41')
set_date()
meter('22_Inverter_44_','inverter','S_Inverter_42','SI42')
set_date()
meter('22_Inverter_45_','inverter','S_Inverter_43','SI43')
set_date()
meter('22_Inverter_46_','inverter','S_Inverter_44','SI44')
set_date()
meter('22_Inverter_47_','inverter','S_Inverter_45','SI45')
set_date()
meter('22_Inverter_48_','inverter','S_Inverter_46','SI46')
set_date()
meter('22_Inverter_49_','inverter','S_Inverter_47','SI47')

set_date()
meter('22_MFM_4_','MFM','LT','LT')
set_date()
meter('22_MFM_3_','MFM','ACDB','ACDB')
set_date()
meter('22_MFM_1_','MFM','SI_MFM','SI_MFM')
set_date()
meter('22_MFM_2_','MFM','CI_MFM','CI_MFM')

set_date()
meter('22_WMS_1_','WMS','WMS','WMS')

print('DONE!')




        

    
