rm(list = ls(all = T))

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}

prepareSumm = function(dataread)
{
  da = nrow(dataread)
  thresh = 5
  gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
  gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
  gsismp = sum(dataread[complete.cases(dataread[,5]),5])/60000
  subdata = dataread[complete.cases(dataread[,3]),]
  subdata = subdata[as.numeric(subdata[,3]) > thresh,]

  tamb = mean(dataread[complete.cases(dataread[,6]),6])
  tambst = mean(subdata[,6])
  
  hamb = mean(dataread[complete.cases(dataread[,7]),7])
  hambst = mean(subdata[,7])
  
  tambmx = max(dataread[complete.cases(dataread[,6]),6])
  tambstmx = max(subdata[,6])
  
  hambmx = max(dataread[complete.cases(dataread[,7]),7])
  hambstmx = max(subdata[,7])
  
  tambmn = min(dataread[complete.cases(dataread[,6]),6])
  tambstmn = min(subdata[,6])
  
  hambmn = min(dataread[complete.cases(dataread[,7]),7])
  hambstmn = min(subdata[,7])
  
  tcab = mean(dataread[complete.cases(dataread[,9]),9])
	tcabsh = mean(subdata[,9])
  hcab = mean(dataread[complete.cases(dataread[,10]),10])
	hcabsh = mean(subdata[,10])
  
  gsirat = gsi1 / gsi2
  smprat = gsismp / gsi2
  daPerc = da/14.4
  datawrite = data.frame(Date = substr(dataread[1,1],1,10),PtsRec = rf(da),GPy = rf(gsi1), GSi = rf(gsi2),Gmod = rf(gsismp),
                         Tamb = rf1(tamb), TambSH = rf1(tambst),TambMx = rf1(tambmx), TambMn = rf1(tambmn),
                         TambSHmx = rf1(tambstmx), TambSHmn = rf1(tambstmn), Hamb = rf1(hamb), HambSH = rf1(hambst),
                         HambMx = rf1(hambmx), HambMn = rf1(hambmn), HambSHmx = rf1(hambstmx), HambSHmn = rf1(hambstmn),
                         TCab = rf1(tcab), TCabSH=rf1(tcabsh),HCab=rf1(hcab),HCabSH= rf1(hcabsh), GsiRat = rf3(gsirat), SpmRat = rf3(smprat),DA=rf1(daPerc),stringsAsFactors=F)
  datawrite
}
rewriteSumm = function(datawrite)
{
  
  df = data.frame(Date = as.character(datawrite[1,1]),GPy = as.character(datawrite[1,3]),Gsi = as.character(datawrite[1,4]),Gmod= as.character(datawrite[1,5]),
             Tamb = as.character(datawrite[1,6]),TambSH = as.character(datawrite[1,7]),Hamb = as.character(datawrite[1,12]),HambSH = as.character(datawrite[,13]),
						 DA=as.character(datawrite[1,24]),stringsAsFactors=F)
  df
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[719]"
pathwrite = "/home/admin/Dropbox/Second Gen/[PH-719S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[PH-719S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("719","PH-719S",days[z])
			
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
