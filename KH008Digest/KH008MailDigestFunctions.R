rm(list = ls())
source("/home/admin/CODE/common/math.R")
TIMESTAMPSALARM = NULL
TIMESTAMPSALARM2 = NULL
TIMESTAMPSALARM3 = NULL
TIMESTAMPSALARM4 = NULL
TIMESTAMPSALARM5 = NULL
TIMESTAMPSALARM7 = NULL
METERCALLED = 0
ltcutoff = .001
CABLOSSTOPRINT = 0
#INSTCAP=c(2835.32,495.95,1397.5,2249.975,2835.32,0,6300)
INSTCAP = c(2835.32, 495.95, 1397.5, 2249.975, 2854.8, 0, 6300)
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

fetchGSIData = function(date)
{
	pathMain = '/home/admin/Dropbox/Second Gen/[KH-008S]'
	yr = substr(date,1,4)
	mon = substr(date,1,7)
	txtFileName = paste('[KH-008S] ',date,".txt",sep="")
	pathyr = paste(pathMain,yr,sep="/")
	gsiVal = NA
	if(file.exists(pathyr))
	{
		pathmon = paste(pathyr,mon,sep="/")
		if(file.exists(pathmon))
		{
			pathfile = paste(pathmon,txtFileName,sep="/")
			if(file.exists(pathfile))
			{
				dataread = read.table(pathfile,sep="\t",header = T)
				gsiVal = as.numeric(dataread[,3])
			}
		}
	}
	return(c(gsiVal))
}


secondGenData = function(filepath,writefilepath)
{
  {
		if(METERCALLED == 1){
	 		TIMESTAMPSALARM <<- NULL
			TIMESTAMPSALARM2 <<- NULL
			TIMESTAMPALARM3 <<- NULL
			TIMESTAMPALARM4 <<- NULL
			TIMESTAMPALARM5 <<- NULL
			TIMESTAMPALARM7 <<- NULL
			}
		else if(METERCALLED == 2){
			TIMESTAMPSALARM2 <<- NULL}
		else if(METERCALLED == 3)
		{
			TIMESTAMPALARM3 <<- NULL
		}
		else if(METERCALLED ==4)
		{
			TIMESTAMPALARM4 <<- NULL
		}
		else if(METERCALLED == 5)
			TIMESTAMPALARM5 <<- NULL
		else if(METERCALLED == 7)
			TIMESTAMPALARM7 <<- NULL
	}
	dataread = try(read.table(filepath,header=T,sep="\t",stringsAsFactors=F),silent=F)
	parts = unlist(strsplit(filepath,"/"))
	stnprint = parts[(length(parts)-1)]
	stnnum = as.numeric(substr(parts[length(parts)],11,11))
	print(paste("stn-id is",stnprint,"mtidx is",stnnum,"instcap is",INSTCAP[stnnum]))
	if(METERCALLED == 6)
	{
		{
			if(class(dataread) == "try-error" || (nrow(dataread) < 1))
			{
				df = data.frame(Date = NA,DA = NA, Eac1 = NA,stringsAsFactors=F)	
			}
			else
			{
				Date = as.character(substr(dataread[1,1],1,10))
				DA = round(nrow(dataread)/14.4,1)
				Eac1 = as.numeric(dataread[,16])
				Eac1 = Eac1[complete.cases(Eac1)]
				{
				if(length(Eac1))
					Eac1 = round(sum(Eac1)/60000,2)
				else
					Eac1 = NA
				}
				df = data.frame(Date=Date, DA=DA,Eac1=Eac1,stringsAsFactors=F)
			}
		}
		write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
		return(df)
	}

	{
	if(class(dataread) == "try-error" || (nrow(dataread) < 1))
	{
		df = data.frame(Date = NA,DA = NA, DT=NA, Eac1 = NA, Eac2=NA, LastRead = NA, LastTime = NA, Yld1 = NA, Yld2=NA,stringsAsFactors=F)	
	}
	else
	{
		idxen = 39
		idxpac = 15
		sign = 1
		if(METERCALLED == 7)
		{
			idxen = 12
			idxpac = 47
			sign = -1
		}

		date = substr(as.character(dataread[1,1]),1,10)
		DA = round(nrow(dataread)/14.4,1)
		Eac1 = Eac2 = LastR = LastT = DT= NA
		tmp = as.numeric(dataread[,idxpac])
		tmp = tmp[complete.cases(tmp)]
		if(length(tmp))
		{
		  Eac1 = round(sum(tmp)/60000,2)*sign
		}
		
		tmpdt = as.numeric(dataread[,idxpac])
		tmp2 = as.character(dataread[,1])
		tmpdt = tmpdt[timetomins(tmp2) >480]
		tmp2 = tmp2[timetomins(tmp2) >480]
		tmpdt = tmpdt[timetomins(tmp2) < 1080]
		tmp2 = tmp2[timetomins(tmp2) < 1080]
		if(length(tmp2))
		{
			base = length(tmp2)
			tmp2 = tmp2[complete.cases(tmpdt)]
			tmpdt = tmpdt[complete.cases(tmpdt)]
			if(length(tmp2))
			{
				tmp2 = tmp2[tmpdt < 1]
				DT = round((100*length(tmp2)/base),1)
				if(length(tmp2))
				{
					if(METERCALLED == 1)
						TIMESTAMPALARM <<- tmp2
					else if(METERCALLED ==2)
						TIMESTAMPALARM2 <<- tmp2
					else if(METERCALLED ==3)
						TIMESTAMPALARM3 <<- tmp2
					else if(METERCALLED ==4)
						TIMESTAMPALARM4 <<- tmp2
					else if(METERCALLED ==5)
						TIMESTAMPALARM5 <<- tmp2
					else if(METERCALLED ==7)
						TIMESTAMPALARM7 <<- tmp2
				}
			}
		}
		Eac2 = NA
		tmp = as.numeric(dataread[,idxen])
		tmp2 = as.character(dataread[,1])
		tmp2 = tmp2[complete.cases(tmp)] #correct, timestamp is based on Eac2 complete.case
		tmp = tmp[complete.cases(tmp)]

		if(length(tmp))
		{
		  Eac2 = round(((tmp[length(tmp)] - tmp[1])/1000),2)
			LastR = as.numeric(tmp[length(tmp)])
			LastT = as.character(tmp2[length(tmp2)])
		}
		Yld1 = round(Eac1/INSTCAP[stnnum],2)
		Yld2 = round(Eac2/INSTCAP[stnnum],2)
		GSi = fetchGSIData(date)
		PR1 = round(Yld1 * 100/GSi,1)
		PR2 = round(Yld2 * 100/GSi,1)

		df = data.frame(Date=date,DA=DA,DT=DT,Eac1=Eac1,Eac2=Eac2,LastRead=LastR,LastTime=LastT,Yld1=Yld1,Yld2=Yld2,
		GSi = GSi, PR1 = PR1, PR2 = PR2, stringsAsFactors=F)
	}
	}
	write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1, filepathm2,filepathm3,filepathm4,filepathm5,filepathm6,filepathm7,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F) #its correct dont change
  dataread2 = read.table(filepathm2,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread3 = read.table(filepathm3,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread4 = read.table(filepathm4,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread5 = read.table(filepathm5,header = T,sep = "\t",stringsAsFactors=F) #its correct dont change
	dataread7 = try(read.table(filepathm7,header = T,sep = "\t",stringsAsFactors=F),silent=T) #its correct dont change
	idxuse = c(1,2,3,4,5) # Meter not yet setup so don't take 7th value
	{
	if(class(dataread7)=="try-error" || nrow(dataread7)==0)
	{
		Eac17 = Eac27 = Yld17 = Yld27 = Last7T = Last7R = PR17 =PR27=NA
	}
	else
	{
		Eac17 = as.numeric(dataread7[1,4])
		Eac27 = as.numeric(dataread7[1,5])
		Yld17 = as.numeric(dataread7[1,8])
		Yld27 = as.numeric(dataread7[1,9])
		PR17 = as.numeric(dataread7[1,11])
		PR27 = as.numeric(dataread7[1,12])
		Last7R = as.numeric(dataread7[1,6])
		Last7T = as.character(dataread7[1,7])
		#idxuse = c(1,2,3,4,5,7) #Use 7th value for total PR/Yld
	}
	}
	MainInt = PercSol = FacLoad = PercWHR = NA

	Eac1Tot = as.numeric(dataread1[1,4]) + as.numeric(dataread2[1,4]) + as.numeric(dataread3[1,4]) + as.numeric(dataread4[1,4]) + as.numeric(dataread5[1,4]) 
	Eac2Tot = as.numeric(dataread1[1,5]) + as.numeric(dataread2[1,5]) + as.numeric(dataread3[1,5]) + as.numeric(dataread4[1,5]) + as.numeric(dataread5[1,5])
	if(class(dataread7)!="try-error")
	{
		#Eac1Tot = Eac1Tot + Eac17
		#Eac2Tot = Eac2Tot + Eac27
	}
	Yld1Tot = round(Eac1Tot / sum(INSTCAP[idxuse]),2)
	Yld2Tot = round(Eac2Tot / sum(INSTCAP[idxuse]),2)
	PR1Tot = format(round(Yld1Tot *100/ as.numeric(dataread1[1,10]),1),nsmall=1)
	PR2Tot = format(round(Yld2Tot * 100/as.numeric(dataread1[1,10]),1),nsmall=1)
	Yld1Tot = format(Yld1Tot,nsmall=2)
	Yld2Tot = format(Yld2Tot,nsmall=2)
	
	covy1 = c(as.numeric(dataread1[1,8]),as.numeric(dataread2[1,8]),as.numeric(dataread3[1,8]),
	as.numeric(dataread4[1,8]),as.numeric(dataread5[1,8]),Yld17)
	covy2 = c(as.numeric(dataread1[1,9]),as.numeric(dataread2[1,9]),as.numeric(dataread3[1,9]),
	as.numeric(dataread4[1,9]),as.numeric(dataread5[1,9]),Yld27)
	
	covy1 = covy1[complete.cases(covy1)]
	covy2 = covy2[complete.cases(covy2)]
	{
		if(length(covy1))
		{
				sdy1 = round(sdp(covy1),3)
				covy1 = format(round(sdy1*100 / mean(sdy1),1),nsmall=1)
				sdy1 = format(sdy1,nsmall=3)
		}
		else
		{
			sdy1 = covy1 = NA
		}
	}

	{
		if(length(covy2))
		{
			sdy2 = round(sdp(covy2),3)
			covy2 = format(round(sdy2*100/mean(sdy2),1),nsmall=1)
			sdy2 = format(sdy2,nsmall=3)
		}
		else
		{
			sdy2 = covy2 = NA
		}
	}
	
	if(!is.na(filepathm6))
	{
		print(filepathm6)
		dataread6 = read.table(filepathm6,header=T,sep="\t",stringsAsFactors=F)
		if(class(dataread6) != "try-error" && nrow(dataread6) != 0)
		{
			MainInt = as.numeric(dataread6[,3])
			FacLoad = MainInt + as.numeric(Eac2Tot)
      if(is.finite(Eac27))
        FacLoad = FacLoad + as.numeric(Eac27)
			PercSol = round(as.numeric(Eac2Tot)*100/FacLoad,1)
      PercWHR = round(as.numeric(Eac27)*100/FacLoad,1)
		}
	}

	df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),
	DA= as.character(dataread1[1,2]),
	DT = as.character(dataread1[1,3]),
	Eac1M1 = as.character(dataread1[1,4]),
	Eac2M1 = as.character(dataread1[1,5]),
	LastReadM1 = as.character(dataread1[1,6]),
	LastTimeM1 = as.character(dataread1[1,7]),
	Yld1M1 = as.character(dataread1[1,8]),
	Yld2M1 = as.character(dataread1[1,9]),
	Eac1M2 = as.character(dataread2[1,4]),
	Eac2M2 = as.character(dataread2[1,5]),
	LastReadM2 = as.character(dataread2[1,6]),
	LastTimeM2 = as.character(dataread2[1,7]),
	Yld1M2 = as.character(dataread2[1,8]),
	Yld2M2 = as.character(dataread2[1,9]),
	Eac1M3 = as.character(dataread3[1,4]),
	Eac2M3 = as.character(dataread3[1,5]),
	LastReadM3 = as.character(dataread3[1,6]),
	LastTimeM3 = as.character(dataread3[1,7]),
	Yld1M3 = as.character(dataread3[1,8]),
	Yld2M3 = as.character(dataread3[1,9]),
	Eac1M4 = as.character(dataread4[1,4]),
	Eac2M4 = as.character(dataread4[1,5]),
	LastReadM4 = as.character(dataread4[1,6]),
	LastTimeM4 = as.character(dataread4[1,7]),
	Yld1M4 = as.character(dataread4[1,8]),
	Yld2M4 = as.character(dataread4[1,9]),
	Eac1M5 = as.character(dataread5[1,4]),
	Eac2M5 = as.character(dataread5[1,5]),
	LastReadM5 = as.character(dataread5[1,6]),
	LastTimeM5 = as.character(dataread5[1,7]),
	Yld1M5 = as.character(dataread5[1,8]),
	Yld2M5 = as.character(dataread5[1,9]),
	Eac1Tot = Eac1Tot,
	Eac2Tot = Eac2Tot,
	Yld1Tot = Yld1Tot,
	Yld2Tot = Yld2Tot,
	PR1Tot = PR1Tot,
	PR2Tot = PR2Tot,
	GSi = as.character(dataread1[1,10]),
	PR1M1 = as.character(dataread1[1,11]),
	PR2M1 = as.character(dataread1[1,12]),
	PR1M2 = as.character(dataread2[1,11]),
	PR2M2 = as.character(dataread2[1,12]),
	PR1M3 = as.character(dataread3[1,11]),
	PR2M3 = as.character(dataread3[1,12]),
	PR1M4 = as.character(dataread4[1,11]),
	PR2M4 = as.character(dataread4[1,12]),
	PR1M5 = as.character(dataread5[1,11]),
	PR2M5 = as.character(dataread5[1,12]),
	SDY1 = sdy1,
	SDY2 = sdy2,
	CovY1 = covy1,
	CovY2 = covy2,
	MainInt = MainInt,
	FacLoad = FacLoad,
	PercSol = PercSol,
	Eac1M7 = as.character(Eac17),
	Eac2M7 = as.character(Eac27),
	LastReadM7 = as.character(Last7R),
	LastTimeM7 = as.character(Last7T),
	Yld1M7 = as.character(Yld17),
	Yld2M7 = as.character(Yld27),
	PR1M7 = as.character(PR17),
	PR2M7 = as.character(PR27),
  PercWHR = as.character(PercWHR),
	stringsAsFactors=F)
  {
    if(file.exists(writefilepath))
    {
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
}

