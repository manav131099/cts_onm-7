rm(list = ls())

path = "/home/admin/Dropbox/Gen 1 Data/[PH-005X]"

years = dir(path)
for(x in 1 : length(years))
{
	pathyears = paste(path,years[x],sep="/")
	mnths = dir(pathyears)
	for(y in 1 : length(mnths))
	{
		pathdays = paste(pathyears,mnths[y],sep="/")
		days = dir(pathdays)
		for(z in 1 : length(days))
		{
			print(days[z])
			pathac = paste(pathdays,days[z],sep="/") #changing the directory paths to read each days file
			dataread = read.table(pathac,header = T,sep = "\t")
			oldcolnames = colnames(dataread)
			dataread[,(ncol(dataread)+1)] = rep(NA,nrow(dataread)) #assigning NA values to the newlly added colomns
			dataread[,(ncol(dataread)+1)] = rep(NA,nrow(dataread)) #assigning NA values to the newlly added colomns
			colnames(dataread) = c(oldcolnames,"Gsi00","Tmod")     # renaming the new columns as Gsi00 and Tmod
			write.table(dataread,file = pathac,row.names = F,col.names = T,sep ="\t",
			append = F)
		}
	}
}

