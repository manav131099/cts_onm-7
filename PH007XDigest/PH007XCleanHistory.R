rm(list = ls())
errHandle = file('/home/admin/Logs/LogsPH007XHistory.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/PH007XDigest/FTPPH007Xdump.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
LTCUTOFF = .001
FIREERRATA = 1
FIRETWILIONA = 0
THRESHOLDLOCK = 10
COUNTERLOCK = 0
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins)/1)+1))
}
checkErrata = function(row,ts)
{
  if(ts < 540 || ts > 1020)
	{return()}
	if(FIREERRATA == 0)
	{
	  print(paste('Errata mail already sent','so no more mails'))
		return()
	}
	{
	if(!is.finite(as.numeric(row[2])))
	{
		FIRETWILIONA <<- FIRETWILIONA + 1
	}
	else
	{
		FIRETWILIONA <<- 0
		{
			if(as.numeric(row[2]) < LTCUTOFF)
				COUNTERLOCK <<- COUNTERLOCK + 1
			else
				COUNTERLOCK <<- 0
		}
	}
	}
	if((FIRETWILIONA > 10) || ((COUNTERLOCK >= THRESHOLDLOCK) && is.finite(as.numeric(row[2]))))
	{
		FIREERRATA <<- 0
		subj = paste('PH-007X','down')
    body = 'Last recorded timestamp and reading\n'
		body = paste(body,'\nTimestamp:',as.character(row[1]))
		body = paste(body,'\n\nReal Power Tot kW reading:',as.character(row[2]))
		if(FIRETWILIONA>10)
		{
		body = paste(body,'\n\nMore than 10 continuous readings are NA')
		}
		rec=getRecipients("PH-007X","a")
		send.mail(from = 'operations@cleantechsolar.com',
							to = rec,
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent '))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
										"\n Real Pow Tot kW reading:",as.character(row[2]))
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "PH-007X"',sep = "")
		system(command)
		print('Twilio message fired')
		recordTimeMaster("PH-007X","TwilioAlert",as.character(row[1]))
		FIRETWILIONA <<-0
		COUNTERLOCK <<-0
	}
}
stitchFile = function(path,days,pathwrite,erratacheck)
{
	x = 1
	t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
	{
		print(paste("Skipping",days[x],"Due to err in file"))
		return()
	}
  dataread = t
	if(nrow(dataread) < 1)
		return()
	idxfetch = -1
  dataread = dataread[,-c(1,2)]
	{
	if(grepl("Gsi00",days[x]))
	{
		idxvals = length(colnames) - 2
		idxfetch = 2
	}
	else if(grepl("Tmod",days[x]))
	{
		idxvals = length(colnames) -1
		idxfetch = 2
	}
	else if(grepl("ComAp",days[x]))
	{
		idxvals = length(colnames) 
		idxfetch = 20
	}
	else
	{
	currcolnames = colnames(dataread)
  idxvals = match(currcolnames,colnames)
	idxvals2 = c(1:length(currcolnames))
	mtch = which(idxvals %in% NA)
	if(length(mtch))
		idxvals2 = idxvals2[-mtch]
  }
	}
	print(paste("days[x] is ",days[x],"idxfetch is",idxfetch))
	print(ncol(dataread))
  for(y in 1 : nrow(dataread))
  {
	  ta = substr(as.character(dataread[y,1]),1,10)
		ta = format(as.Date(ta,'%m-%d-%Y'),'%Y-%m-%d')
		sp = unlist(strsplit(as.character(dataread[y,1])," "))
		dataread[y,1] = paste(ta,sp[2])
		idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
		idxvals = idxvals[complete.cases(idxvals)]
		if(length(idxvals) < 1)
		{
			print(paste("Skipping row",y,"for",days[x],"since idxvals length is 0"))
		}
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    rowtemp2 = rowtemp
		rowtemp2 = unlist(rowtemp2)
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname,"] ",day,".txt",sep="")
    pathtowrite = paste(pathwritemon,filename,sep="/")
		{
		if(grepl("Gsi00",days[x]) || grepl("Tmod",days[x]) || grepl("ComAp",days[x]))
		{
			rowtemp2[idxvals] = as.numeric(dataread[y,idxfetch])
			rowtemp2[1] = as.character(dataread[y,1])
		}
		else
		{
			rowtemp2[idxvals] = dataread[y,idxvals2]
		}
		}
  {
    if(!file.exists(pathtowrite))
    {
      df = data.frame(rbind(rowtemp2),stringsAsFactors = F)
      colnames(df) = colnames
      if(idxts != 1)
        {
          df[idxts,] = rowtemp2
          df[1,] = rbind(unlist(rowtemp))
        }
				FIREERRATA <<- 1
				FIRETWILIONA <<-0
				COUNTERLOCK <<-0
    }
    else
    {
      df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      {
			if(idxts > nrow(df))
			{
				df[idxts,]=rowtemp2
			}
			else
			{
			if(as.character(rowtemp2[1])==as.character(df[idxts,1]))
			{
			if(grepl("Gsi00",days[x]) || grepl("Tmod",days[x]) || grepl("ComAp",days[x]))
				df[idxts,idxvals]=dataread[y,idxfetch]#dont replace with rowtemp2 this is correct
			else
				df[idxts,(1:(length(rowtemp2)-3))] = rowtemp2[1:(length(rowtemp2)-3)]
			}
			else
			{
				print(paste('Mismatch Old time',as.character(df[idxts,1]),
				'New time',as.character(rowtemp2[1])))
				df[(idxts+1):(nrow(df)+1),] = df[idxts:nrow(df),]
				df[idxts,]=rowtemp2
			}
			}
			}
    }
  }
	if(erratacheck != 0 && (!(grepl('Gsi00',days[x]) || grepl("Tmod",days[x]) || grepl("ComAp",days[x]))))
	{
	  pass = c(as.character(df[idxts,1]),as.character(as.numeric(df[idxts,15])/1000))
	  checkErrata(pass,idxts)
	}
	df = df[complete.cases(as.character(df[,1])),]
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  tab = try(write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F),silent=T)
	if(class(tab)=='try-error')
	{
		print('Error writing table')
		print(df)
	}
	print('Done stitching file')
}
recordTimeMaster("PH-007X","Gen1",as.character(df[nrow(df),1]))
}
path = '/home/admin/Data/Episcript-CEC/EGX PH007X Dump'
checkdir(path)
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[PH-007X]'
pathdatelastread = '/home/admin/Start'  #last data read path
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'PH007X.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
lastrecordeddate = c('','','','')
idxtostart = c(1,1,1,1)
{
	if(!file.exists(pathdatelastread))
	{
	  print('Last date read file not found so cleansing system from start')
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		#lastrecordeddate = c(lastrecordeddate[1])
		days1 = days[grepl('PH-007',days)]
		days2 = days[grepl('Gsi00',days)]
		days3 = days[grepl('Tmod',days)]
		days4 = days[grepl('ComAp',days)]
		idxtostart[1] = match(lastrecordeddate[1],days1)
		idxtostart[2] = match(lastrecordeddate[2],days2)
		if(length(lastrecordeddate)>2)
		idxtostart[3] = match(lastrecordeddate[3],days3)
		idxtostart[4] = match(lastrecordeddate[4],days4)
		print(paste('Date read is',lastrecordeddate,'and idxtostart is',idxtostart))
	}
}
checkdir(pathwrite)
idxtomatchph = match(TRUE,grepl("PH-007",days))
colnames = colnames(read.csv(paste(path,days[idxtomatchph],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
colnames = c(colnames,"Gsi00","Tmod","ActivePower")
rowtemp = unlist(rep(NA,(length(colnames))))
x=1
stnname =  "[PH-007X"   #why no ] here 
days1 = days[grepl('PH-007',days)] #searching through the lines for regex match for 3 cols as Gsi00 , Tmod,Comap in file PH007
days2 = days[grepl('Gsi00',days)]
days3 = days[grepl('Tmod',days)]
days4 = days[grepl('ComAp',days)]
{
	if(idxtostart[1] <= length(days1))
	{
		print(paste('idxtostart[1] is',idxtostart[1],'while length of days1 is',length(days1)))
		for(x in idxtostart[1] : length(days1))
		{
 		 stitchFile(path,days1[x],pathwrite,0)
		}
		lastrecordeddate[1] = as.character(days1[x])
		print('Meter 1 DONE')
	}
	if(idxtostart[2] <= length(days2))
	{
		print(paste('idxtostart[2] is',idxtostart[2],'while length of days2 is',length(days2)))
		for(x in idxtostart[2] : length(days2))
		{
 		 stitchFile(path,days2[x],pathwrite,0)
		}
		lastrecordeddate[2] = as.character(days2[x])
		print('Meter 2 DONE')
	}
	if(idxtostart[3] <= length(days3) && length(days3))
	{
		print(paste('idxtostart[3] is',idxtostart[3],'while length of days3 is',length(days3)))
		for(x in idxtostart[3] : length(days3))
		{
 		 stitchFile(path,days3[x],pathwrite,0)
		}
		lastrecordeddate[3] = as.character(days3[x])
		print('Meter 2 DONE')
	}
	if(idxtostart[4] <= length(days4) && length(days4))
	{
		print(paste('idxtostart[4] is',idxtostart[4],'while length of days3 is',length(days4)))
		for(x in idxtostart[4] : length(days4))
		{
 		 stitchFile(path,days4[x],pathwrite,0)
		}
		lastrecordeddate[4] = as.character(days4[x])
		print('Meter 2 DONE')
	}
	else if( (!(idxtostart[1] < length(days1))) && (!(idxtostart[2] < length(days2))))
	{
		print('No historical backlog')
	}
}
print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
write(lastrecordeddate,file =pathdatelastread)
ERRATAFIRE=0
while(1)
{
	print('Checking FTP for new data')
 	filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
  daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]    #what is csv ; the file format ?
	if(length(daysnew) == length(days))
	{
		if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
		print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,ERRATAFIRE)
		print(paste('Done',daysnew[x]))
	}
	daysnew2 = daysnew[grepl('Gsi00',daysnew)]
	daysnew3 = daysnew[grepl('Tmod',daysnew)]
	daysnew4 = daysnew[grepl('ComAp',daysnew)]
	daysnew = daysnew[grepl('PH-007',daysnew)]
	if(length(daysnew))
	lastrecordeddate[1] = as.character(daysnew[length(daysnew)]) #storing lines corresponding to PH-007
	if(length(daysnew2))
	lastrecordeddate[2] = as.character(daysnew2[length(daysnew2)]) #storing lines corresponding to Tmod and ComAp and Gsi00 such that
	if(length(daysnew3))
	lastrecordeddate[3] = as.character(daysnew3[length(daysnew3)])
	if(length(daysnew4))
	lastrecordeddate[4] = as.character(daysnew4[length(daysnew4)])
	write(c(lastrecordeddate[1],lastrecordeddate[2],lastrecordeddate[3],lastrecordeddate[4]),pathdatelastread)
	gc()
	ERRATAFIRE = 1
}
print('Exit Loop')
sink()
