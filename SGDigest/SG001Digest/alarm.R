rm(list = ls())
doneforthedayfiringpr = prless50 = prless60 = 0
lastsuccesspacts = " "
pathtstwilioread = "/Users/liuchenxi/Desktop/file/SG-001.txt"


timetomins = function(x)
{
  splitup = unlist(strsplit(x,":"))
  hr = as.numeric(splitup[1])
  min = as.numeric(splitup[2])
  return(((hr * 60)+min+1))
}


#convert time in format 'DD-MM-YYYY HH:MM' to a numerical value between 1-1140
timetomins2 = function(x)
{
  lists = unlist(strsplit(x,"\\ "))
  seq1 = seq(from = 2, to = length(lists),by=2)
  lists = lists[seq1]
  lists = unlist(strsplit(lists,":"))
  seq1 = seq(from = 1, to = length(lists),by = 2)
  seq2 = seq(from = 2, to = length(lists),by = 2)
  hr = as.numeric(lists[seq1])
  min = as.numeric(lists[seq2])
  return((hr * 60)+min+1)
}

firetwilio = function(day)
{
  if(doneforthedayfiringpr)
  {
    print(paste('Done for the day firing twilio',day))
    return()
  }
  
  dataread = read.table(day,header = T,sep="\t",stringsAsFactors = F)
  datareadac = dataread
  
  tmmins = timetomins2(as.character(dataread[,1]))
  dataread2 = dataread[tmmins>540,]
  tmmins = tmmins[tmmins>540]
  dataread = dataread2[tmmins < 1020,]
  
  #if no values are between this time-window return
  if(nrow(dataread)<1)
  {
    print(paste('Not yet time...Last time:',as.character(datareadac[nrow(datareadac),1])))
    return()
  }
  fulldataread = dataread
  
  #extract subdata from index of lastsuccesspacts to the end of the file
  if(lastsuccesspacts != " ")
  {
    print(paste('to match',lastsuccesspacts,'with',as.character(dataread[nrow(dataread),1])))
    idxtsmtch = match(lastsuccesspacts,as.character(dataread[,1]))
    print(paste('match val of idxts is',idxtsmtch))
    print(paste('nrow dataread',nrow(dataread)))
    
    #if the match is the last row, then no new data exists so return the call
    if((idxtsmtch+1) > nrow(dataread))
    {
      print('No new data recorded.... returning twilio call')
      return()
    }
    
    #Extract the sub-data
    dataread = dataread[(idxtsmtch+1):nrow(dataread),]
  }
  
  
  #set last successpacts to the last recorded value and write this to a file
  lastsuccesspacts <<- as.character(dataread[nrow(dataread),1])
  print(paste('Last timestamp recordered sucess',lastsuccesspacts))
  write(lastsuccesspacts,file=pathtstwilioread,append = F)

  #calculate Live-PR
  pr = (sum(as.numeric(dataread[complete.cases(dataread[,15]),15]))*100)/((sum(as.numeric(dataread[complete.cases(dataread[,42]),42]))/60000)*44.785)
  
  #check for PR < 25 and PR > 100 but less than 500
  if(pr < 25 || ((pr > 100) && pr < 500))
  {
    message = paste("Station SG-001X PR err, PR:",as.character(pr),"Last Timestamp:",as.character(datareadtrue[nrow(datareadtrue),1]))
    command = paste('python /home/admin/CODE/Misc/SG_001.py "',message,'"')
    system(command)
    sendEmailToo(message,"SG-001X PR Error");
    
    #set doneforthedayfiring flag
    doneforthedayfiringpr <<-1
    print(paste('PR less than 25 error, message fired, time',as.character(dataread[nrow(dataread),1])))
    
    #update the lastfiredtspr time to current time
    lastfiredtspr25 <<- Sys.time()
    return()
  }
  
  if(pr < 50)
  {
    print(paste('Pr less than 50, time',as.character(dataread[nrow(dataread),1])))
    
    #update the prless50 counter
    prless50 <<- prless50 + 1
    
    #check if counter is > 2
    if(prless50 > 2)
    {
      
      #if yes fire warning message
      message = paste("Station SG-001X PR err, PR less than 50 for 3 continuous readings, PR for day so far:",as.character(pr),"Last Timestamp:",as.character(datareadtrue[nrow(datareadtrue),1]))
      command = paste('python /home/admin/CODE/Misc/SG_001.py "',message,'"')
      system(command)
      sendEmailToo(message,"SG-001X PR Error");
      
      #update flag
      doneforthedayfiringpr <<-1
      print(paste('PR less than 50 error, message fired, time',as.character(dataread[nrow(dataread),1])))
      
      #update last recorded time
      lastfiredtspr50 <<- Sys.time()
      return()
    }
  }
  
  if(pr < 60)
  {
    print(paste('Pr less than 60, time',as.character(dataread[nrow(dataread),1])))
    if(pr > 50)
    {
      #since pr > 50 reset the prless50 counter
      prless50 <<- 0
      print(paste('Pr greater than 50 so reset pr50 counter, time',as.character(dataread[nrow(dataread),1])))
    }
    
    #increment the prless60 counter
    prless60 <<- prless60 + 1
    
    if(prless60 > 4)
    {
      #if yes fire warning message
      message = paste("Station SG-001X PR err, PR less than 60 for 5 continuous readings, PR for day so far:",as.character(pr),"Last Timestamp:",as.character(datareadtrue[nrow(datareadtrue),1]))
      command = paste('python /home/admin/CODE/Misc/IN_001.py "',message,'"')
      system(command)
      sendEmailToo(message,"SG-001X PR Error");
      
      #update flag
      doneforthedayfiringpr <<-1
      print(paste('Pr less than 60 error, message fired time',as.character(dataread[nrow(dataread),1])))
      
      #update last recorded time-stamp
      lastfiredtspr60 <<- Sys.time()
    }
    return()
  }
  
  #if none of the condition are met reset the psless50 and prless60 values as the PR > 60
  prless50 <<- 0
  prless60 <<- 0
  
}


manageflags = function()
{
  tmnow = Sys.time()
  if(doneforthedayfiringpr)
  {
    print('Checking if pr flag has to be reset')
    {
      if(!(prless50 > 2 || prless60 > 4))
      {
        print('Flag set due to PR < 25, so checking if it has to be reset')
        print(paste('time pr flag set',as.character(lastfiredtspr25),'Time now',as.character(tmnow)))
        {
          if(abs(as.numeric(difftime(tmnow,lastfiredtspr25,units="mins")))> 60)
          {
            print('Time is more than 1 hr so resetting flag pr')
            doneforthedayfiringpr <<-0
            prless50 <<- 0
            prless60 <<- 0 
          }
          else
            print('Time not yet elapsed, so not resetting flag')
        }
      }
      else if(prless50 > 2)
      {
        print('Flag set due to PR < 50, so checking if it has to be reset')
        print(paste('time pr flag set',as.character(lastfiredtspr50),'Time now',as.character(tmnow)))
        {
          if(abs(as.numeric(difftime(tmnow,lastfiredtspr50,units="mins")))> 60)
          {
            print('Time is more than 1 hr so resetting flag pr')
            doneforthedayfiringpr <<-0
            prless50 <<- 0
            prless60 <<- 0 
          }
          else
            print('Time not yet elapsed, so not resetting flag')
        }
      }
      else if(prless60 > 4)
      {
        print('Flag set due to PR < 60, so checking if it has to be reset')
        print(paste('time pr flag set',as.character(lastfiredtspr60),'Time now',as.character(tmnow)))
        {
          if(abs(as.numeric(difftime(tmnow,lastfiredtspr60,units="mins")))> 60)
          {
            print('Time is more than 1 hr so resetting flag pr')
            doneforthedayfiringpr <<-0
            prless50 <<- 0
            prless60 <<- 0 
          }
          else
            print('Time not yet elapsed, so not resetting flag')
        }
      }
    }
  }
  print(paste('Manage flag operations done, donefiring pr is',doneforthedayfiringpr))
}
day <- "/Users/liuchenxi/Dropbox/Gen 1 Data/[SG-001X]/2017/2017-03/[SG-001X] 2017-03-04.txt"
firetwilio("/Users/liuchenxi/Dropbox/Gen 1 Data/[SG-001X]/2017/2017-03/[SG-001X] 2017-03-04.txt")
