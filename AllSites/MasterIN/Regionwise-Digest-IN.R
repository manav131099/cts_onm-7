rm(list = ls())
handle1=file("/home/admin/Logs/Logs-Regionwise_Market_Digest.txt",open='wt',encoding='UTF-8')
handle="/home/admin/Dropbox/Second Gen/"
sourcehandle="/home/admin/CODE/fleximail_digest/mail.py"
sitesumm="/home/admin/CODE/AllSites/MasterIN/IN_Plus_Flexi_Allsite.xls"
handle2="/home/admin/Dropbox/FlexiMC_Data/DailyMail/Attachments/"
sink(handle1,type='message',append = T)
library(reticulate)
library(readxl)
library(mailR)
library(tidyr)
library(geosphere)  #Need to install "geospehere" package for region mapping on spatial distance
source('/home/admin/CODE/common/math.R')
mailbody=function(sa,handle,sitesumm,handle2)
{

  ## Region Co-ordinates
  #Latitude
  l1=c(13.082680,11.016844,14.442599,17.385044,18.520430,19.876165,19.997453,22.307159,25.321377,26.912434,28.704059,30.487389,21.319388,14.470586)
  #Longtitude
  l2=c(80.237617,76.955832,79.986456,78.486671,73.856744,75.343314,73.789802,73.181219,74.586953,75.787271,77.102490,76.800009,76.222427,75.914154)
  #Region Names
  regionname=c("Chennai","Coimbatore","Nellore","Hyderabad","Pune","Aurangabad","Nasik","Vadodara","Bhilwara","Jaipur","Delhi","Lalru","Burhanpur","Davangere")

  atn=c()
  atnn1=c()
  charset=c()
  atnn=0
  e=e1=NA


  #yla1=yla2=yla3=yla4=yla5=yla6=yla7=yla8=yla9=yla10=yla11=yla12=yla13=yla14=pra1=pra2=pra3=pra4=pra5=pra6=pra7=pra8=pra9=pra10=pra11=pra12=pra13=pra14=c()
  #yla=list(yla1,yla2,yla3,yla4,yla5,yla6,yla7,yla8,yla9,yla10,yla11,yla12,yla13,yla14)
  #pra=list(pra1,pra2,pra3,pra4,pra5,pra6,pra7,pra8,pra9,pra10,pra11,pra12,pra13,pra14)
  #exist=c(existce,existco,existne,existh,existp,exista,existna,existva,existbi,existj,existd,existl,existbu,existda)
  #exist=c()


  site=c()
  sitename=c()
  syssize=c()
  region=c()
  location=c()
  bodymain=c()
  s=as.Date(sa)
  dz<-read_excel(sitesumm)
  dty=separate(dz,4,c('Lat','Long'),",")

  for (regname in regionname)
  {
    charsett=substr(as.character(regname),1,3)
    charset=c(charset,as.character(charsett))
  }

  #Region Mapping K-means Clustering
  for (e in 1:nrow(dty))
  {
    dist=c()
    lat=as.numeric(dty[e,4])
    long=as.numeric(dty[e,5])
    for (er in 1:length(l1))
    {
      dist=c(dist,as.numeric(distm (c(l2[er],l1[er]), c(long,lat), fun = distHaversine)))
    }
    min=as.numeric(min(dist))

    for (er in 1:length(dist))
    {
      if(dist[er]==min)
        qwr=er
    }
    region=c(region,as.character(charset[qwr]))
  }


  yla=vector(mode = "list",length = length(regionname))
  pra=vector(mode = "list",length = length(regionname))
  exist=numeric(length(regionname))

  message(paste("Enter for Digest for ",as.character(s)))

  for (qw in 1:(nrow(dz)))
  {
    site=c(site,as.character(dz[qw,1]))
    sitename=c(sitename,as.character(dz[qw,2]))
    syssize=c(syssize,as.numeric(dz[qw,5]))
    location=c(location,as.character(dz[qw,3]))

  }

  body=""
  bodytemp=""
  for (asdf in 1:length(regionname))
  {
    bodytemp=paste("\n\n---------------------------------------------------------------------------------","\n\n",as.character(regionname[asdf]),"\n\n","---------------------------------------------------------------------------------",sep = "")
    bodymain=c(bodymain,as.character(bodytemp))
  }



  bodyf=""
  bodyl=""
  bodyf = paste(bodyf,"Market: India\n\n",sep="")
  bodyf = paste(bodyf,"Number of Systems: ",nrow(dz),sep="")
  sysno=0
  for (t in 1:length(syssize))
  {
    if(syssize[t]>0)
      sysno=sysno+1
  }
  bodyf = paste(bodyf,"\n\nNumber of Active Systems: ",sysno,sep="")
  for (rt in 1:nrow(dz))
  {
    path=handle
    pathaa=handle2
    indicator=0

    bodyy=""
    e=e1=c()
    pattern=as.character(site[rt])
    sizee=as.numeric(syssize[rt])
    exception=c("IN-036S","IN-015S","IN-011C")
    if(sizee>0)
    {
      for (excptn in exception)
      {
        if(as.character(site[rt])==as.character(excptn))
          indicator=1
      }


      if(indicator==1)
      {
        if(as.character(site[rt])=="IN-036S")
        {
          bodyy = paste(bodyy,"\n\n---------------------------------------------------------------------------------",sep="")
          bodyy = paste(bodyy,"\n",site[rt]," ", sitename[rt],", ",location[rt]," ", syssize[rt]," ", "kWp",sep="")
          bodyy = paste(bodyy,"\n---------------------------------------------------------------------------------",sep="")
          pather=handle
          path=paste(pather,"[IN-036S]","/",sep="")
          pather=paste(path,substr(s,1,4),"/",substr(s,1,7),"/",sep="")

          names=list.files(pather)
          pat=grepl(s,names)
          n1=NA
          for(i in 1:length(pat))
          {
            if(isTRUE(pat[i]))
            {
              n1=as.character(names[i])
            }
          }
          pather=paste(pather,n1,sep="")

          if(length(names)>0)
          {
            if(file.exists(pather))
            {
              df<-read.table(pather,header = TRUE,sep = "\t")
              eac=as.numeric(df[1,21])+as.numeric(df[1,22])
              yld=mean(c(as.numeric(df[1,34]),as.numeric(df[1,35])))
              pr=(yld/as.numeric(df[1,30]))*100
              bodyy = paste(bodyy,"\n\nSystem generated [kWh]: ",formatC(as.numeric(eac),format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")
              bodyy = paste(bodyy,"\n\nDaily specific Yield [kWh/kWp]: ",formatC(as.numeric(yld),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
              bodyy = paste(bodyy,"\n\nPerfomance Ratio [%]: ",formatC(as.numeric(pr),format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")
              e=as.numeric(yld)
              e1=as.numeric(pr)
              if(!is.na(pr)){
                if(as.numeric(pr)<60)
                  atn=site[rt]
              }
            }
            else
              bodyy = paste(bodyy,"\n\nData Not Available",sep="")
          }
          else
            bodyy = paste(bodyy,"\n\nData Not Available for a Long Time",sep="")

        }
        if(as.character(site[rt])=="IN-011C")
        {
          bodyy = paste(bodyy,"\n\n---------------------------------------------------------------------------------",sep="")
          bodyy = paste(bodyy,"\n",site[rt]," ", sitename[rt],", ",location[rt]," ", syssize[rt]," ", "kWp",sep="")
          bodyy = paste(bodyy,"\n---------------------------------------------------------------------------------",sep="")
          file=paste("Digest[",pattern,"]_",s,".xls",sep = "")
          pathC=paste(handle2,file,sep = "")

          if(file.exists(pathC))
          {
            df<-read_excel(pathC,skip = 1)
            col=colnames(df)

            totMFM=as.numeric(df[1,40])
            bodyy = paste(bodyy,"\n\nEnergy Generated [kWh]: ",formatC(totMFM,format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")
            yldMFM=totMFM/as.numeric(syssize[rt])
            if(!is.finite(yldMFM))
              yldMFM=0
            bodyy = paste(bodyy,"\n\nDaily Specific Yield [kWh/kWp]: ",formatC(as.numeric(yldMFM),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")


            perrat=(yldMFM/as.numeric(df[1,39]))*100
            if(!is.finite(perrat))
              perrat=0
            bodyy = paste(bodyy,"\n\nPerfomance Ratio [%]: ",formatC(as.numeric(perrat),format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")
            if(!is.na(perrat))
            {
              if(as.numeric(perrat)<60)
                atn=c(atn,site[rt])
            }

            if(!is.na(yldMFM))
              e=c(e,as.numeric(yldMFM))
            if(!is.na(yldMFM))
              e1=c(e1,as.numeric(perrat))
          }
          else
            bodyy = paste(bodyy,"\n\nData Not Available",sep="")
        }
        if(as.character(site[rt])=="IN-015S")
        {
          bodyy = paste(bodyy,"\n\n---------------------------------------------------------------------------------",sep="")
          bodyy = paste(bodyy,"\n",site[rt]," ", sitename[rt],", ",location[rt]," ", syssize[rt]," ", "kWp",sep="")
          bodyy = paste(bodyy,"\n---------------------------------------------------------------------------------",sep="")
          pather=handle
          path=paste(pather,"[IN-015S]","/",sep="")
          pather=paste(path,substr(s,1,4),"/",substr(s,1,7),"/",sep="")

          names=list.files(pather)
          pat=grepl(s,names)
          n1=NA
          for(i in 1:length(pat))
          {
            if(isTRUE(pat[i]))
            {
              n1=as.character(names[i])
            }
          }
          pather=paste(pather,n1,sep="")

          if(length(names)>0)
          {
            if(file.exists(pather))
            {
              df<-read.table(pather,header = TRUE,sep = "\t")
              eac=as.numeric(df[1,22])+as.numeric(df[1,23])
              hvyld=as.numeric(df[1,23])/1814.4
              lvyld=as.numeric(df[1,22])/1814.4
              prlvmod =as.numeric(lvyld)*100/as.numeric(df[1,34])
              prhvmod =as.numeric(hvyld)*100/as.numeric(df[1,34])

              yld=mean(c(as.numeric(hvyld),as.numeric(lvyld)))
              pr=mean(c(as.numeric(prlvmod),as.numeric(prhvmod)))
              bodyy = paste(bodyy,"\n\nSystem generated [kWh]: ",formatC(as.numeric(eac),format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")
              bodyy = paste(bodyy,"\n\nDaily specific Yield [kWh/kWp]: ",formatC(as.numeric(yld),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
              bodyy = paste(bodyy,"\n\nPerfomance Ratio [%]: ",formatC(as.numeric(pr),format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")
              e=as.numeric(yld)
              e1=as.numeric(pr)
              if(!is.na(pr))
              {
                if(as.numeric(pr)<60)
                  atn=site[rt]
              }
            }
            else
              bodyy = paste(bodyy,"\n\nData Not Available",sep="")
          }
          else
            bodyy = paste(bodyy,"\n\nData Not Available for a Long Time",sep="")

        }
      }
      if(indicator==0)
      {
        if(grepl("[C]$",pattern,ignore.case = T))
        {
          bodyy = paste(bodyy,"\n\n---------------------------------------------------------------------------------",sep="")
          bodyy = paste(bodyy,"\n",site[rt]," ", sitename[rt],", ",location[rt]," ", syssize[rt]," ", "kWp",sep="")
          bodyy = paste(bodyy,"\n---------------------------------------------------------------------------------",sep="")
          file=paste("Digest[",pattern,"]_",s,".xls",sep = "")
          pathC=paste(handle2,file,sep = "")

          if(file.exists(pathC))
          {
            df<-read_excel(pathC,skip = 1)
            col=colnames(df)
            y=grepl("Specific Yield",col,ignore.case = T)
            p=grepl("Performance ratio",col,ignore.case = T)
            p1=grepl("Irradiation",col,ignore.case = T)
            p3=grepl("Irradiance",col,ignore.case = T)
            ec1=grepl("MFM - Enery Export \\(KWh\\)",col,ignore.case = T)
            ec3=grepl("MFM - Energy Export \\(KWh\\)",col,ignore.case = T)
            ec2=grepl("Enery Export \\(KWh\\)",col,ignore.case = T)
            ec4=grepl("Energy Export \\(KWh\\)",col,ignore.case = T)
            ec5=grepl("Total - Energy Export \\(KWh\\)",col,ignore.case = T)
            ec6=grepl("Total - Enery Export \\(KWh\\)",col,ignore.case = T)
            ec7=grepl("MFM- Enery Export \\(KWh\\)",col,ignore.case = T)
            ec8=grepl("MFM- Energy Export \\(KWh\\)",col,ignore.case = T)
            ec9=grepl("MFM-Enery Export \\(KWh\\)",col,ignore.case = T)
            ec10=grepl("MFM-Energy Export \\(KWh\\)",col,ignore.case = T)
            gh=gh1=gh3=NA
            gh2=c()
            for (i in 1:length(p1))
            {
              if(isTRUE(p1[i]))
              {
                gh3=i
              }
              else
              {
                if(isTRUE(p1[i]))
                  gh3=i
              }
            }
            for (i in 1:length(y))
            {
              if(isTRUE(y[i]))
              {
                gh=i
              }

            }
            for(i in 1:length(p))
            {
              if(isTRUE(p[i]))
              {
                gh1=i
              }
            }
            for (i in 1:length(ec1))
            {
              if(isTRUE(ec1[i]) || isTRUE(ec2[i]) || isTRUE(ec3[i]) || isTRUE(ec4[i]) || isTRUE(ec7[i]) || isTRUE(ec8[i]) || isTRUE(ec9[i]) || isTRUE(ec10[i]))
              {
                gh2=c(gh2,i)
              }
            }
            qwerty=0
            if(length(gh2)>1)
            {

              for (jkl in 1:length(ec5))
              {
                if(isTRUE(ec5[jkl]) || isTRUE(ec6[jkl]))
                  qwerty=1
              }
              totMFM=0
              for(ert in 1:length(gh2))
              {
                totMFM=totMFM+as.numeric(df[1,gh2[ert]])
              }
              if(qwerty==1)
                totMFM=totMFM/2
              if(qwerty==0)
                totMFM=totMFM
              bodyy = paste(bodyy,"\n\nEnergy Generated [kWh]: ",formatC(totMFM,format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")

            }
            else
            {
              if(length(gh2)==1)
              {
                totMFM=0
                for(ert in 1:length(gh2))
                {
                  totMFM=totMFM+as.numeric(df[1,gh2[ert]])
                }
                bodyy = paste(bodyy,"\n\nEnergy Generated [kWh]: ",formatC(totMFM,format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")
              }
            }

            #if(!is.na(gh))
            yldMFM=totMFM/as.numeric(syssize[rt])
            bodyy = paste(bodyy,"\n\nDaily Specific Yield [kWh/kWp]: ",formatC(as.numeric(yldMFM),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")

            if(!is.na(gh3))
            {
              perrat=(yldMFM/as.numeric(df[1,gh3]))*100
              bodyy = paste(bodyy,"\n\nPerfomance Ratio [%]: ",formatC(as.numeric(perrat),format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")
              if(!is.na(perrat))
              {
                if(as.numeric(perrat)<60)
                  atn=c(atn,site[rt])
              }
            }
            if(!is.na(gh))
              e=c(e,as.numeric(df[1,gh]))
            if(!is.na(gh1))
              e1=c(e1,as.numeric(df[1,gh1]))
          }
          else
            bodyy = paste(bodyy,"\n\nData Not Available",sep="")
        }
        else
        {
          bodyy = paste(bodyy,"\n\n---------------------------------------------------------------------------------",sep="")
          bodyy = paste(bodyy,"\n",site[rt]," ", sitename[rt],", ",location[rt]," ", syssize[rt]," ", "kWp",sep="")
          bodyy = paste(bodyy,"\n---------------------------------------------------------------------------------",sep="")
          path=paste(path,"[",site[rt],"]","/",sep="")
          path=paste(path,substr(s,1,4),"/",substr(s,1,7),"/",sep="")
          dirs=c()
          dir=list.dirs(path,full.names = F,recursive = F)
          if(length(dir)>0)
          {
            for (i in 1:length(dir))
            {
              if(!grepl("^[.]",dir[i]))
              {
                dirs=c(dirs,dir[i])
              }
            }
            dirs=dirs[!is.na(dirs)]
            if(length(dirs)<1)
            {
              bodyy = paste(bodyy,"\n\nNo Data",sep="")
            }
            if(length(dirs)>0)
            {
              totyld=c()
              totpwr=0
              totpr=c()
              for (ij in 1:length(dirs))
              {
                path1=paste(path,as.character(dirs[ij]),"/",sep="")
                names=list.files(path1)
                wrty=length(names)
                wrtyi=0
                if(length(names)>0)
                {
                  pat=grepl(s,names)
                  n1=NA
                  for(i in 1:length(pat))
                  {
                    if(isTRUE(pat[i]))
                    {
                      n1=as.character(names[i])
                    }
                  }
                  path2=paste(path1,n1,sep="")
                  if(file.exists(path2))
                  {
                    wrtyi=1
                    df<-read.table(path2,header = TRUE,sep = "\t")
                    col=colnames(df)
                    y1=grepl("Yield2",col,ignore.case = T)
                    y2=grepl("DailySpecYield",col,ignore.case = T)
                    p=grepl("PR2",col,ignore.case = T)
                    ec1=grepl("Eac2",col,ignore.case = T)
                    ec2=grepl("EacM2",col,ignore.case = T)
                    gh=gh1=gh2=NA
                    for (i in 1:length(y2))
                    {
                      if(isTRUE(y2[i]))
                      {
                        gh=i
                      }
                      else
                      {
                        if(isTRUE(y1[i]))
                          gh=i
                      }
                    }
                    for(i in 1:length(p))
                    {
                      if(p[i]==TRUE)
                      {
                        gh1=i
                      }
                    }
                    for (i in 1:length(ec1))
                    {
                      if(isTRUE(ec1[i]))
                      {
                        gh2=i
                      }
                      else
                      {
                        if(isTRUE(ec2[i]))
                          gh2=i
                      }
                    }

                    if(!is.na(gh2))
                      totpwr=totpwr+as.numeric(df[1,gh2])
                    if(!is.na(gh))
                      totyld=c(totyld,as.numeric(df[1,gh]))
                    if(!is.na(gh1))
                      totpr=c(totpr,as.numeric(df[1,gh1]))


                  }
                  else
                    bodyy = paste(bodyy,"\n\nData Not Available for ",dirs[ij],sep="")
                }
              }
              if(wrty>0)
              {
                if(wrtyi==1)
                {
                  totyld=totyld[!is.na(totyld)]
                  totpr=totpr[!is.na(totpr)]
                  bodyy = paste(bodyy,"\n\nEnergy Generated [kWh]: ",formatC(as.numeric(totpwr),format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")
                  bodyy = paste(bodyy,"\n\nDaily specific Yield [kWh/kWp]: ",formatC(as.numeric(mean(totyld)),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
                  if(length(totpr)>0)
                    bodyy = paste(bodyy,"\n\nPerfomance Ratio [%]: ",formatC(as.numeric(mean(totpr)),format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")

                  if(length(totpr)>0){
                    if(mean(totpr)<60)
                      atn=c(atn,as.character(site[rt]))
                  }

                  e=as.numeric(mean(totyld))
                  e1=as.numeric(mean(totpr))
                }
              }
              else
                bodyy = paste(bodyy,"\n\nData Not Available for a Long Time for ",sep="")

            }
          }
          if(length(dir)<1)
          {
            names=list.files(path)

            if(length(names)>0)
            {
              pat=grepl(s,names)
              n1=NA
              for(i in 1:length(pat))
              {
                if(isTRUE(pat[i]))
                {
                  n1=as.character(names[i])
                }
              }
              path1=paste(path,n1,sep="")

              if(file.exists(path1))
              {
                df<-read.table(path1,header = TRUE,sep = "\t")
                col=colnames(df)
                y1=grepl("Yield2",col,ignore.case = T)
                y2=grepl("DailySpecYield",col,ignore.case = T)
                p=grepl("PR2",col,ignore.case = T)
                ec1=grepl("Eac2",col,ignore.case = T)
                ec2=grepl("EacM2",col,ignore.case = T)
                gh=gh1=gh2=NA
                for (i in 1:length(y2))
                {
                  if(isTRUE(y2[i]))
                  {
                    gh=i
                  }
                  else
                  {
                    if(isTRUE(y1[i]))
                      gh=i
                  }
                }
                for(i in 1:length(p))
                {
                  if(isTRUE(p[i]))
                  {
                    gh1=i
                  }
                }
                for (i in 1:length(ec1))
                {
                  if(isTRUE(ec1[i]))
                  {
                    gh2=i
                  }
                  else
                  {
                    if(isTRUE(ec2[i]))
                      gh2=i
                  }
                }
                if(!is.na(gh2))
                  bodyy = paste(bodyy,"\n\nEnergy Generated [kWh]: ",formatC(as.numeric(df[1,gh2]),format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")
                if(!is.na(gh))
                  bodyy = paste(bodyy,"\n\nDaily specific Yield [kWh/kWp]: ",formatC(as.numeric(df[1,gh]),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
                if(!is.na(gh1))
                {
                  bodyy = paste(bodyy,"\n\nPerfomance Ratio [%]: ",formatC(as.numeric(df[1,gh1]),format="f",digits = 1,big.mark = ",",big.interval = 3L),sep="")
                  if(!is.na(df[1,gh1])){
                    if(as.numeric(df[1,gh1])<60)
                      atn=c(atn,as.character(site[rt]))
                  }
                }
                if(!is.na(gh))
                  e=as.numeric(df[1,gh])
                if(!is.na(gh1))
                  e1=as.numeric(df[1,gh1])
              }
              else
                bodyy = paste(bodyy,"\n\nData Not Available",sep="")
            }
            else
              bodyy = paste(bodyy,"\n\nData Not Available for a Long Time",sep="")
          }

        }
      }
    }
    for(asd in 1:length(exist))
    {
      if(as.character(region[rt])==as.character(charset[asd]))
      {

        bodymain[asd]=paste(as.character(bodymain[asd]),bodyy,sep = "")
        ylaa=as.numeric(yla[[asd]])
        praa=as.numeric(pra[[asd]])
        ylaa=c(ylaa,e)
        praa=c(praa,e1)
        yla[[asd]]=ylaa
        pra[[asd]]=praa
        exist[asd]=1
      }
    }


  }


  bodyl = paste(bodyl,"\n\n---------------------------------------------------------------------------------",sep="")
  bodyl = paste(bodyl,"\n---------------------------------------------------------------------------------",sep="")


  for (rt in 1:length(yla))
  {
    if(length(yla[[rt]])>0)
    {

      ylaa=yla[[rt]]
      praa=pra[[rt]]
      ylaa=ylaa[!is.na(ylaa)]
      praa=praa[!is.na(praa)]
      yla[[rt]]=ylaa
      pra[[rt]]=praa
    }

  }

  for (rty in 1:length(yla))
  {

    if(length(yla[[rty]])>1)
      atnn1=c(atnn1,(sdp(yla[[rty]])*100)/mean(yla[[rty]]))
  }
  atnn1=atnn1[!is.na(atnn1)]

  for (rty in 1:length(atnn1))
  {

    if(length(atnn1[rty])>0)
    {
      if(atnn1[rty]>10)
        atnn=rty
    }
  }


  if(length(atn)>0)
  {
    bodyf=paste(bodyf,"\n\nPerformance Ratio low for sites:",sep = "")
    for (i in 1:length(atn))
    {
      message(atn[i])
      bodyf=paste(bodyf,atn[i],",",sep = " ")
    }
  }
  else
    bodyf=paste(bodyf,"\n\nPerformance Ratio low for sites [<60%]: NIL",sep = "")

  for (rty in 1:length(yla))
  {
    if(length(yla[[rty]])>1)
    {
      bodyyy1=""
      bodyyy1 = paste(bodyyy1,"\n\n---------------------------------------------------------------------------------",sep="")
      bodyyy1 = paste(bodyyy1,"\n\nStdDev [Yield]: ",formatC(sdp(as.numeric(yla[[rty]])),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
      bodyyy1 = paste(bodyyy1,"\n\nCoV [Yield]: ",formatC((sdp(as.numeric(yla[[rty]]))*100)/mean(as.numeric(yla[[rty]])),format="f",digits = 2,big.mark = ",",big.interval = 3L)," [%]",sep="")
      #bodyyy1 = paste(bodyyy1,"\n\nStdDev [PR]: ",formatC(as.numeric(sdp(pra[[rty]])),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
      #bodyyy1 = paste(bodyyy1,"\n\nCoV [PR]: ",formatC((as.numeric(sdp(pra[[rty]]))*100)/mean(as.numeric(pra[[rty]])),format="f",digits = 2,big.mark = ",",big.interval = 3L)," [%]",sep="")

      bodymain[rty]=paste(as.character(bodymain[rty]),bodyyy1,sep = "")


    }

  }


  bodyl = paste(bodyl,"\n\n---------------------------------------------------------------------------------",sep="")
  bodyl = paste(bodyl,"\n\nInstalled Capacity [kWp]: ",formatC(sum(syssize),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
  bodyl = paste(bodyl,"\n\nAverage System Size [kWp]: ",formatC(mean(syssize),format="f",digits = 2,big.mark = ",",big.interval = 3L),sep="")
  emer=atn
  bodyfinal=bodyf
  for (i in 1:length(exist))
  {
    if(exist[i]==1)
      bodyfinal=paste(bodyfinal,as.character(bodymain[i]),sep = "")
  }

  body=paste(bodyfinal,bodyl,sep = "")
  reg=regionname[atnn]
  b=list(body,atnn,reg)
  message("Mail Digest Done")
  return(b)
}

download=function(sourcehandle,hnd)
{
  sh=sourcehandle
  outputdir=hnd
  source_python(sh)
  fromemail="saradindu.sengupta@cleantechsolar.com"
  #Attachment removal
  del=list.files(hnd,full.names = TRUE)
  if(length(del)>0)
  {
    for (d in del)
    {
      sys=paste('rm -rf ',d,sep="")
      system(sys)
    }
  }
  #For mail download
  server="imap.gmail.com"
  user="cleantechsolarom@gmail.com"
  password="Saradindu2@"
  emailid=fromemail
  body1=""
  err1=0
  imap=connect(server, user, password)
  m=imap[[1]]
  if(as.character(imap[[2]])!="OK")
  {
    body1=paste(body1,"\n************ Unable to establish link to the mail server **************\n\n",sep = "")
    body1=paste(body1,"\n************ Failed to download FlexiMC data sheets **************\n\n",sep = "")
    err1=1
  }
  downloaAttachmentsInEmail(m, emailid, outputdir)
  listfiles=list.files(outputdir,pattern = "*.xls")
  if(length(listfiles)<1)
  {
    body1=paste(body1,"\nUnable download FlexiMC data sheet.Making sure the mail is there....")
    err1=1
  }
  b=list(body1,err1)
  return(b)
}

#Mail Section
sender <- "operations@cleantechsolar.com"
uname <- 'shravan.karthik@cleantechsolar.com'
pwd = 'CTS&*(789'
recipients <- c("om-india@cleantechsolar.com","rupesh.baker@cleantechsolar.com","rohit.jaswal@cleantechsolar.com")
sendToken = 0
forceSendToken = 0
while (1)
{
  datePrev=as.character(as.Date(substr(format(Sys.time(),tz="Singapore"),1,10))-1)
  sa=datePrev
  hourNow = format(Sys.time(),tz = "Singapore")
  hrNow = as.numeric(substr(hourNow,12,13))
  if(hrNow == 1 || hrNow == 2)
    sendToken = 1

  if(((hrNow == 7 || hrNow == 8) && sendToken) || forceSendToken)
  {
    message(format(Sys.time(),tz=Sys.timezone()))
    message("Download Section")
    body1=download(sourcehandle,handle2)
    if(body1[[2]]==1)
      message("Error in Downloading Attachments for FlexiMC data sheet")
    message("Mail Section")
    body=mailbody(sa,handle,sitesumm,handle2)
    sub=paste("[",as.character(sa),"]"," India [Master] All Site Summary", sep = "")
    #if(body[[2]]>=1)
      #sub=paste("[",as.character(sa),"]"," <WARNING- ",as.character(body[[3]])," Region> India [Master] All Site Summary", sep = "")

    body2=paste(body1[[1]],body[[1]],sep="")
    send.mail(from = sender,
              to = recipients,
              subject = sub,
              body = body2,
              smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
              authenticate = TRUE,
              send = TRUE,
              debug = F)
    message("Mail Sent")
    lastmail=sa
    message(paste("Last mail sent for: ",lastmail,sep=""))
  }
  message("Going to sleep...")
  Sys.sleep(60*60)
}
sink()
