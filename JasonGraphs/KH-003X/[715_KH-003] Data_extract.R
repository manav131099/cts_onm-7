rm(list=ls(all =TRUE))

require('compiler')
enableJIT(3)
pathRead <- "/home/admin/Dropbox/Cleantechsolar/1min/[715]"
pathwritetxt <- "/home/admin/Jason/cec intern/results/715/[715]_summary.txt"
pathwritecsv <- "/home/admin/Jason/cec intern/results/715/[715]_summary.csv"

setwd(pathRead)
filelist <- dir(pattern = ".txt", recursive= TRUE)
timemin <- format(as.POSIXct("2016-10-03 06:59:59"), format="%H:%M:%S")
timemax <- format(as.POSIXct("2017-09-17 17:00:00"), format="%H:%M:%S")

stationLimit <- function(date){
  if (date == as.Date("2016-10-06")){
    return (492.8)
  }
  else if (date < as.Date("2016-10-20")){
    return (1126.4)
  }
  else if (date < as.Date("2016-10-25")){
    return (1504)
  }
  else if (date < as.Date("2016-10-27")){
    return (1734.4)
  }
  else if (date < as.Date("2016-11-07")){
    return (2265.6)
  }
  else if (date < as.Date("2016-11-26")){
    return (2476.8)
  }
  else {
    return (2624)
  }
  
}
nameofStation <- "KH-003A"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()
col5 <- c()
col6 <- c()
col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = col4[10^6] = col5[10^6] = col6[10^6] = 0
index <- 1

for (i in filelist[-(1:3)]){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")
  date <- substr(i,20,29)
  col0[index] <- nameofStation
  col1[index] <- date
  
  #data availability
  pts <- length(temp[,1])
  col3[index] <- pts
  
  #pac
  if (date != "2016-10-17"){
    condition <- format(as.POSIXct(temp[,1]), format="%H:%M:%S") > timemin &
      format(as.POSIXct(temp[,1]), format="%H:%M:%S") <timemax
    pacTemp <- temp[condition,]
    pacTemp <- pacTemp[complete.cases(pacTemp[,24]),]
    pac <- pacTemp[,24]
    numer <- length(pac[pac>0.1])
    pacTemp2 <- pacTemp[pac<0.1,]
    pacTemp2 <- pacTemp2[pacTemp2[,3]>20,]
    denom <- length(pacTemp2[,1])+numer
    flagRate <- numer/denom
    col4[index] <- flagRate*100
    
    
    #wpac
    gsi <- pacTemp[,3]
    flag <- as.numeric(pac<0.1 & gsi > 20)
    wgsi <- gsi/sum(gsi)
    downloss <- wgsi * flag * 100
    if (length(downloss)>0){
      col5[index] <- 100 - sum(downloss)
    }
    else{
      col5[index] <- NA
    }
    
    #pr
    gsi <- temp[,3]
    eac <- temp[,86][complete.cases(temp[,86])]
    eac <- eac[eac < 10^8]
    diff <- eac[length(eac)]- eac[1]
    pr <- diff/stationLimit(as.character.Date(date))/sum(gsi)*60000
    if (length(pr)==0){
      col6[index] = NA
    }
    else{
      col6[index] <- pr*100
    }
  }
  else{
    col4[index] = NA
    col5[index] = NA
    col6[index] = NA
  }
  #ex_proc <- index/length(filelist)*100
  print(paste(i, "done"))
  index <- index + 1
  
}

col0 <- col0[1:(index-1)]
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col4 <- col4[1:(index-1)]
col5 <- col5[1:(index-1)]
col6 <- col6[1:(index-1)]
col2 <- col3/max(col3)*100
col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col5[is.na(col5)] <- NA
col6[is.na(col6)] <- NA

result <- cbind(col0,col1,col2,col3,col4,col5,col6)

result[,3] <- round(as.numeric(result[,3]),1)
result[,5] <- round(as.numeric(result[,5]),1)
result[,6] <- round(as.numeric(result[,6]),1)
result[,7] <- round(as.numeric(result[,7]),1)

rownames(result) <- NULL
result <- data.frame(result)
#result <- result[-length(result[,1]),]
dataWrite <- result
colnames(result) <- c("Meter Reference","Date","Data Availability [%]",
                      "No. of Points", "Uptime [%]","Weighted Uptime [%]",
                      "Performance Ratio [%]")
write.table(result,na = "",pathwritetxt,row.names = FALSE,sep ="\t")
write.csv(result,na = "",pathwritecsv,row.names = FALSE)

